package test;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import api.TwitchAPI;
import connection.IRC;
import events.client.MessageListener;
import models.api.users.User;
import models.client.ChatMessage;
import models.api.streams.*;

import org.joda.time.Days;
import org.joda.time.Seconds;
import org.json.JSONArray;
import org.json.JSONObject;

public class Test {
	
	public static void main(String[] args) throws UnknownHostException, IOException, UnirestException, InterruptedException, ExecutionException{
		//TwitchAPI api = new TwitchAPI("s7ka1ne3rrzngcxr3pmd0zlpw2bvgcn");
		//String id = Integer.toString(TwitchAPI.Users.getUserByName("hsdogdog").getUsers()[0].getId());
		//Stream stream = TwitchAPI.Streams.getStreamByUser(id);
		//int seconds = Seconds.secondsBetween(stream.getCreatedAt(), common.Parsing.convertToDateTime(new org.joda.time.DateTime(org.joda.time.DateTimeZone.UTC).toString())).getSeconds();
		//System.out.println(common.Parsing.getTimeOutput(seconds));
		//System.exit(0);
		IRC connection = new IRC("sato_chat", "oauth:u53r9or2zqejkdknxf5314cd2wpupz", true);
		
		//Add event handler class
		EventHandler events = new EventHandler();
		connection.onMessageReceived(events);
		
		connection.connect();
		connection.joinChannel("satokaheni");
	}
}

class EventHandler implements MessageListener{

	@Override
	public void onMessageReceived(Object sender, ChatMessage e) {
		System.out.println(String.format("Channel %s \t%s: %s", e.getChannel(), e.getUsername(), e.getMessage()));
	}
	
}
