package common;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import models.client.JoinedChannel;

public class Parsing {
	//get message type
	public static String getMsgType(String message) {
		if (message.contains("tmi.twitch.tv ")) {
			return message.split("tmi.twitch.tv ")[1].split(" ")[0];
		}
		else
			return "";
	}
	
	//Detect if new sub
	public static boolean detectNewSub(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("USERNOTICE"))
			return (message.split(";")[7].split("=")[1] == "sub");
		
		return false;
	}
	
	//Detect if resub
	public static boolean detectResub(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("USERNOTICE"))
			return (message.split(";")[7].split("=")[1] == "resub");
		
		return false;
	}
	
	//Detect if message
	public static boolean detectMessage(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("PRIVMSG"))
			return true;
		
		return false;
	}
	
	//Detect if command
	public static boolean detectCommand(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("PRIVMSG")) {
			String msg = message.split("PRIVMSG ")[1].split(" ")[1].substring(1);
			if (msg.charAt(0) == '!')
				return true;
		}
		
		return false;
	}
	
	//Detect if Ping
	public static boolean detectPING(String message) {
		return (message.equalsIgnoreCase("PING :tmi.twitch.tv"));
	}
	
	//Detect User Join
	public static boolean detectUserJoin(String message) {
		String msgtype = getMsgType(message);
		return (msgtype.equalsIgnoreCase("join"));
	}
	
	//Detect User Part
	public static boolean detectUserPart(String message) {
		String msgType = getMsgType(message);
		return (msgType.equalsIgnoreCase("part"));
	}
	
	//Detect Clearchat
	public static boolean detectClearChat(String message) {
		return (message.split("#")[0] == ":tmi.twitch.tv CLEARCHAT");
	}
	
	//Detect User Ban
	public static boolean detectUserBan(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("CLEARCHAT")) {
			return (message.split(";")[0].split("=")[0].equalsIgnoreCase("@ban-reason"));
		}
		return false;
	}
	
	//Detect User Timeout
	public static boolean detectUserTimeout(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("CLEARCHAT")) {
			return (message.split(";")[0].split("=")[0].equalsIgnoreCase("@ban-duration"));
		}
		return false;
	}
	
	//Detect Moderator Join
	public static boolean detectModeratorJoin(String message) {
		if (message.split(" #")[0] == ":jtv MODE") {
			return (message.split(" ")[3] == "+o");
		}
		return false;
	}
	
	//Detect Moderator Join
	public static boolean detectModeratorLeave(String message) {
		if (message.split(" #")[0] == ":jtv MODE") {
			return (message.split(" ")[3] == "-o");
		}
		return false;
	}
	
	//Detect Roomstate change
	public static boolean detectRoomstate(String message) {
		String msgType = getMsgType(message);
		return (msgType.equalsIgnoreCase("ROOMSTATE"));
	}
	
	//Detect UserState change
	public static boolean detectUserState(String message) {
		String msgType = getMsgType(message);
		return (msgType.equalsIgnoreCase("USERSTATE"));
	}
	
	//Detect Host Start
	public static boolean detectHostStart(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("HOSTTARGET"))
			return !(message.split(" ")[3] == ":-");
		return false;
	}
	
	//Detect Host Stop
	public static boolean detectHostStop(String message) {
		String msgType = getMsgType(message);
		if (msgType.equalsIgnoreCase("HOSTTARGET"))
			return (message.split(" ")[3] == ":-");
		return false;
	}

	//Detect someone hosting
	public static boolean detectNowHosting(String line, HashMap<String, JoinedChannel> joinedChannels) {
		String msgType = getMsgType(line);
		if (msgType.equalsIgnoreCase("NOTICE")) {
			if (line.contains("msg-id=host_on")) {
				if (line.contains(":Now hosting"))
					return (joinedChannels.containsKey(line.split("#")[1].split(" ")[0]));
			}
		}
		return false;
	}
	
	//Detect Join Finish
	public static boolean detectJoinChannelCompleted(String line) {
		if (!line.contains(" ") || !line.split(" ")[1].equalsIgnoreCase("366"))
			return false;
		
		return true;
	}
	
	//Detect another channel hosting 
	public static boolean detectBeingHosted(String line) {
		return (line.contains(" ") && line.split(" ")[1] == "PRIVMSG" && line.contains("jtv!jtv@jtv") && line.contains("is now hosting you"));
	}
	
	//Parse string into datetime
	public static DateTime convertToDateTime(String time) {
		if (time.split("[.]").length > 1)
			return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ").parseDateTime(time);
		else
			return DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").parseDateTime(time);
	}
	
	//Convert seconds to the correct hours minutes and seconds
	public static String getTimeOutput(int seconds) {
		String output = "";
		int hours = seconds/3600;
		int minutes = (seconds/60) % 60;
		seconds = seconds % 60;
		if (hours > 1) 
			output += Integer.toString(hours) + " hours ";
		else if (hours == 1)
			output += Integer.toString(hours) + " hour ";
		
		if (minutes > 1)
			output += Integer.toString(minutes) + " minutes ";
		else if (minutes == 1)
			output += Integer.toString(minutes) + " minute ";
		
		if (seconds > 1)
			output += Integer.toString(seconds) + " seconds";
		else if (seconds == 1)
			output += Integer.toString(seconds) + " second";
		
		return output;
	}
}
