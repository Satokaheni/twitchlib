package common;

import java.util.ArrayList;

public class Helpers {
	
	//Parse quoted messages into list
	public static ArrayList<String> parseQuotedMessage(String message) {
		ArrayList<String> args = new ArrayList<String>();
		
		//check if encountered a quote
		boolean quote = (message.charAt(0) == '"');			
		for(String arg : message.split("\"")) {
			//Check if quoted
			if(quote) {
				args.add(arg);
				quote = false;
				continue;
			}
			
			//if not a quote split into their own arguments
			for(String micro : arg.split(" ")) {
				//whitepace or empty
				if (micro.trim().isEmpty())
					continue;
				
				args.add(micro);
				quote = true;
			}
		}
		
		
		return args;
	}
	
}
