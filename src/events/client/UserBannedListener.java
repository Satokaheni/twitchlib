package events.client;

import models.client.UserBan;

public interface UserBannedListener {
	public void onUserBan(Object sender, UserBan e);
}
