package events.client;

import models.client.ChannelState;

public interface ChannelStateChangedListener {
	public void onChannelStateChanged(Object sender, ChannelState cs);
}
