package events.client;

import models.client.UserState;

public interface UserStateChangedListener {
	public void onUserStateChange(Object sender, UserState e);
}
