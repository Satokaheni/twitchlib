package events.client;

import models.client.UserTimeout;

public interface UserTimeoutListener {
	public void onUserTimeout(Object sender, UserTimeout e);
}
