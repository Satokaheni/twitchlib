package events.client;

import models.client.Subscriber;

public interface SubListener {
	public void onNewSubscriber(Object sender, Subscriber e);
}
