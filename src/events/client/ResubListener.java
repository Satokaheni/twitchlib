package events.client;

import models.client.Subscriber;

public interface ResubListener {
	public void onResubscriber(Object sender, Subscriber e);
}
