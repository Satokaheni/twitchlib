package events.client;

import models.client.Moderator;

public interface ModeratorPartListener {
	public void onModeratorLeave(Object sender, Moderator m);
}
