package events.client;

public interface ClearChatListener {
	public void onChatCleared(Object sender, String channel);
}
