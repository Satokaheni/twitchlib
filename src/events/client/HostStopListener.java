package events.client;

import models.client.HostStop;

public interface HostStopListener {
	public void onHostingStopped(Object sender, HostStop e);
}
