package events.client;

import models.client.UserJoined;

public interface UserJoinedListener {

	public void onUserJoined(Object sender, UserJoined e);
}
