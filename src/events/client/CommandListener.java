package events.client;

import models.client.ChatCommand;

public interface CommandListener {
	public void onCommandReceived(Object sender, ChatCommand e);
}
