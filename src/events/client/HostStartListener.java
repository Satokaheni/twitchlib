package events.client;

import models.client.HostStart;

public interface HostStartListener {
	public void onHostStarted(Object sender, HostStart e);
}
