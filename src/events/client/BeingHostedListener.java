package events.client;
import models.client.BeingHosted;

public interface BeingHostedListener {
	public void onBeingHosted(Object sender, BeingHosted args);
}
