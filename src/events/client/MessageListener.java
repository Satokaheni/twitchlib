package events.client;

import models.client.ChatMessage;

public interface MessageListener {
	public void onMessageReceived(Object sender, ChatMessage e);
}
