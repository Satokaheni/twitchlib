package events.client;

import models.client.UserLeft;

public interface UserLeftListener {

	public void onUserLeft(Object sender, UserLeft e);
	
}
