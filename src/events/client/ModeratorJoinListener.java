package events.client;

import models.client.Moderator;

public interface ModeratorJoinListener {
	public void onModeratorJoin(Object sender, Moderator m);
}
