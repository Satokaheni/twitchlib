package events.client;

import models.client.NowHosting;

public interface NowHostingListener {
	public void onNowHosting(Object sender, NowHosting arg);
}
