package events.services;

public interface ThrottleResetListener {
	public void onThrottleReset(Object sender, int duration);
}
