package events.services;

public interface MessageThrottledListener {
	public void onMessageThrottled(Object sender, String message);
}
