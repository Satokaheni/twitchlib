package services;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import events.services.*;

public class MessageThrottler {

	private int messagesAllowed;
	private int interval_period;
	private int currentMessages;
	private ScheduledExecutorService timer;
	private MessageThrottledListener messageThrottledListener;
	private ThrottleResetListener throttleResetListener;
	
	
	//Constructor
	public MessageThrottler(int messagesAllowed, int interval_period) {
		this.messagesAllowed = messagesAllowed;
		this.interval_period = interval_period;
		
		timer = null;
		currentMessages = 0;
	}
	
	//Check if a message is permitted
	public boolean messagePermitted(String message) {
		//Start timer if not started
		if (timer.equals(null)) {
			timer = Executors.newSingleThreadScheduledExecutor();
			timer.scheduleAtFixedRate(() -> { currentMessages = 0; throttleResetListener.onThrottleReset(this, interval_period); timer.shutdown(); timer = null; }, 0, interval_period, TimeUnit.SECONDS);
		}
		
		if ( currentMessages == messagesAllowed) {
			messageThrottledListener.onMessageThrottled(this, message);
			return false;
		}
		
		currentMessages++;
		return true;
		
	}

	//Add listeners
	public void onMessageThrottled(MessageThrottledListener listener) {
		this.messageThrottledListener = listener;
	}
	
	public void onThrottleReset(ThrottleResetListener listener) {
		this.throttleResetListener = listener;
	}
	
	//Accessor Methods
	public int getMessagesAllowed() {
		return messagesAllowed;
	}
	
	public int getIntervalPeriod() {
		return interval_period;
	}
	
	//Setter Methods
	public void setMessagesAllowed(int messages) {
		this.messagesAllowed = messages;
	}
	
	public void setIntervalPeriod(int period) {
		this.interval_period = period;
	}
}
