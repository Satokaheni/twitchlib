package models.api.ingests;

import org.json.JSONArray;

public class Ingests {

	private Ingest[] ingestServers;
	
	public Ingests(JSONArray ingests) {
		parseIngests(ingests);
	}

	//Parse Json
	private void parseIngests(JSONArray ingests) {
		ingestServers = new Ingest[ingests.length()];
		for (int i = 0; i < ingestServers.length; i++)
			ingestServers[i] = new Ingest(ingests.getJSONObject(i));
	}
	
	//Accessor Method
	public Ingest[] getIngestServers() {
		return ingestServers;
	}

}
