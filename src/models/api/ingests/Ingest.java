package models.api.ingests;

import org.json.JSONObject;

public class Ingest {

	private int id;
	private double availability;
	private boolean Default;
	private String name;
	private String url;
	
	public Ingest(JSONObject ingest) {
		parseIngest(ingest);
	}

	//Parse Json
	private void parseIngest(JSONObject ingest) {
		id = ingest.getInt("_id");
		availability = ingest.getDouble("availability");
		Default = ingest.getBoolean("default");
		name = ingest.getString("name");
		url = ingest.getString("url");
	}
	
	//Accessor Methods
	public int getId() {
		return id;
	}
	
	public double getAvailability() {
		return availability;
	}
	
	public boolean getDefault() {
		return Default;
	}
	
	public String getName() {
		return name;
	}
	
	public String getURL() {
		return url;
	}

}
