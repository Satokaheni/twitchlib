package models.api.channelfeed;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class FeedPostReactionPost {

	private DateTime created_at;
	private String emote_id;
	private String id;
	private User user;
	
	public FeedPostReactionPost(JSONObject post) {
		parsePost(post);
	}

	//Parse Json
	private void parsePost(JSONObject post) {
		created_at = common.Parsing.convertToDateTime(post.getString("created_at"));
		emote_id = post.getString("emote_id");
		id = post.getString("id");
		user = new User(post.getJSONObject("user"));
	}
	
	//Accessor Methods
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public String getEmoteId() {
		return emote_id;
	}
	
	public String getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}

}
