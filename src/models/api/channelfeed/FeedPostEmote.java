package models.api.channelfeed;

import org.json.JSONObject;

public class FeedPostEmote {

	private int end;
	private int id;
	private int set;
	private int start;
	
	public FeedPostEmote(JSONObject emote) {
		parseEmote(emote);
	}

	//Parse Json
	private void parseEmote(JSONObject emote) {
		end = emote.getInt("end");
		id = emote.getInt("id");
		set = emote.getInt("set");
		start = emote.getInt("start");
	}

	//Accessor Method
	public int getEnd() {
		return end;
	}
	
	public int getId() {
		return id;
	}
	
	public int getSet() {
		return set;
	}
	
	public int getStart() {
		return start;
	}
}
