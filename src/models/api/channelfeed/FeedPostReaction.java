package models.api.channelfeed;

import org.json.JSONObject;

public class FeedPostReaction {

	private int count;
	private String emote;
	private int[] user_ids;
	
	public FeedPostReaction(JSONObject reaction) {
		parseReaction(reaction);
	}

	//Parse Json
	private void parseReaction(JSONObject reaction) {
		count = reaction.getInt("count");
		emote = reaction.getString("emote");
		user_ids = new int[reaction.getJSONArray("user_ids").length()];
		for (int i = 0; i < user_ids.length; i++) 
			user_ids[i] = reaction.getJSONArray("user_ids").getInt(i);
	}

	//Accessor Methods
	public int getCount() {
		return count;
	}
	
	public String getEmote() {
		return emote;
	}
	
	public int[] getUserIds() {
		return user_ids;
	}
}
