package models.api.channelfeed;

import org.json.JSONObject;

public class FeedPostComments {
	
	private String cursor;
	private int total;
	private FeedPostComment[] comments;

	public FeedPostComments(JSONObject c) {
		parseComments(c);
	}

	//Parse Json
	private void parseComments(JSONObject c) {
		cursor = c.getString("_cursor");
		total = c.getInt("_total");
		comments = new FeedPostComment[c.getJSONArray("comments").length()];
		for (int i = 0; i < comments.length; i++)
			comments[i] = new FeedPostComment(c.getJSONArray("comments").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getCursor() {
		return cursor;
	}
	
	public int getTotal() {
		return total;
	}
	
	public FeedPostComment[] getComments() {
		return comments;
	}
}
