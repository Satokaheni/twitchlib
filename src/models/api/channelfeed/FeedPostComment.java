package models.api.channelfeed;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class FeedPostComment {

	private String body;
	private DateTime created_at;
	private boolean deleted;
	private FeedPostEmote[] emotes;
	private String id;
	private HashMap<String, Boolean> permissions = new HashMap<String, Boolean>();
	private HashMap<String, FeedPostReaction> reactions = new HashMap<String, FeedPostReaction>();
	private User user;
	
	public FeedPostComment(JSONObject comment) {
		parseComment(comment);
	}
	
	//Parse Json
	private void parseComment(JSONObject comment) {
		body = comment.getString("body");
		created_at = common.Parsing.convertToDateTime(comment.getString("created_at"));
		deleted = comment.getBoolean("deleted");
		emotes = new FeedPostEmote[comment.getJSONArray("emotes").length()];
		for (int i = 0; i < emotes.length; i++)
			emotes[i] = new FeedPostEmote(comment.getJSONArray("emotes").getJSONObject(i));
		
		Iterator<?> it = comment.getJSONObject("permissions").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			permissions.put(key, comment.getJSONObject("permissions").getBoolean(key));
		}
		
		it = comment.getJSONObject("reactions").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			reactions.put(key, new FeedPostReaction(comment.getJSONObject("reactions").getJSONObject(key)));
		}
		
		user = new User(comment.getJSONObject("user"));
	}
	
	//Accessor Methods
	public String getBody() {
		return body;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public boolean getDeleted() {
		return deleted;
	}
	
	public FeedPostEmote[] getEmotes() {
		return emotes;
	}
	
	public String getId() {
		return id;
	}
	
	public HashMap<String, Boolean> getPermissions() {
		return permissions;
	}
	
	public HashMap<String, FeedPostReaction> getReactions() {
		return reactions;
	}
	
	public User getUser() {
		return user;
	}

}
