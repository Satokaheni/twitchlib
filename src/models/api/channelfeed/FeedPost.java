package models.api.channelfeed;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class FeedPost {

	private String body;
	private FeedPostComments comments;
	private DateTime created_at;
	private boolean deleted;
	private Object embeds;
	private FeedPostEmote[] emotes;
	private String id;
	private HashMap<String, Boolean> permissions = new HashMap<String, Boolean>();
	private HashMap<String, FeedPostReaction> reactions = new HashMap<String, FeedPostReaction>();
	private User user;
	
	public FeedPost(JSONObject p) {
		parsePost(p);
	}

	private void parsePost(JSONObject p) {
		body = p.getString("body");
		comments = new FeedPostComments(p.getJSONObject("comments"));
		created_at = common.Parsing.convertToDateTime(p.getString("created_at"));
		deleted = p.getBoolean("deleted");
		embeds = p.get("embeds");
		emotes = new FeedPostEmote[p.getJSONArray("emotes").length()];
		for (int i = 0; i < emotes.length; i++) 
			emotes[i] = new FeedPostEmote(p.getJSONArray("emotes").getJSONObject(i));
		
		id = p.getString("id");
		Iterator<?> it = p.getJSONObject("permissions").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			permissions.put(key, p.getJSONObject("permissions").getBoolean(key));
		}
		
		it = p.getJSONObject("reactions").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			reactions.put(key, new FeedPostReaction(p.getJSONObject("reactions").getJSONObject(key)));
		}
		
		user = new User(p.getJSONObject("user"));
	}
	
	//Accessor Methods
	public String getBody() {
		return body;
	}
	
	public FeedPostComments getComments() {
		return comments;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public boolean getDeleted() {
		return deleted;
	}
	
	public Object getEmbeds() {
		return embeds;
	}
	
	public FeedPostEmote[] getEmotes() {
		return emotes;
	}
	
	public String getId() {
		return id;
	}
	
	public HashMap<String, Boolean> getPermissions() {
		return permissions;
	}
	
	public HashMap<String, FeedPostReaction> getReactions() {
		return reactions;
	}
	
	public User getUser() {
		return user;
	}

}
