package models.api.channelfeed;

import org.json.JSONObject;

public class MultipleFeedPosts {

	private String cursor;
	private String topic;
	private boolean disabled;
	private FeedPost[] posts;
	
	public MultipleFeedPosts(JSONObject feed) {
		parseFeed(feed);
	}

	//Parse Json
	private void parseFeed(JSONObject feed) {
		cursor = feed.getString("_cursor");
		topic = feed.getString("_topic");
		disabled = feed.getBoolean("_disabled");
		posts = new FeedPost[feed.getJSONArray("posts").length()];
		for (int i = 0; i < posts.length; i++) 
			posts[i] = new FeedPost(feed.getJSONArray("posts").getJSONObject(i));
	}
	
	//Acccessor Methods
	public String getCursor() {
		return cursor;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public boolean getDisabled() {
		return disabled;
	}
	
	public FeedPost[] getPosts() {
		return posts;
	}

}
