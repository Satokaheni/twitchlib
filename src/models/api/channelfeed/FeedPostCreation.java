package models.api.channelfeed;

import org.json.JSONObject;

public class FeedPostCreation {

	private FeedPost post;
	private String tweet;
	
	public FeedPostCreation(JSONObject p) {
		parsePost(p);
	}

	//parse Json
	private void parsePost(JSONObject p) {
		post = new FeedPost(p.getJSONObject("post"));
		tweet = p.getString("tweet");
	}
	
	//Accessor Method
	public FeedPost getPost() {
		return post;
	}
	
	public String getTweet() {
		return tweet;
	}

}
