package models.api.videos;

import org.json.JSONObject;

public class VideoChannel {

	private String id;
	private String display_name;
	private String name;
	
	public VideoChannel(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		display_name = object.getString("display_name");
		name = object.getString("name");
	}

	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getName() {
		return name;
	}
}
