package models.api.videos;

import org.json.JSONObject;

public class TopVideos {
	
	private Video[] vods;
	
	public TopVideos(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		vods = new Video[object.getJSONArray("vods").length()];
		for (int i = 0; i < vods.length; i++)
			vods[i] = new Video(object.getJSONArray("vods").getJSONObject(i));
	}

	//Accesor Method
	public Video[] getVODS() {
		return vods;
	}
}
