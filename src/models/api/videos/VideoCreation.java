package models.api.videos;

import org.json.JSONObject;

public class VideoCreation {

	private VideoUpload upload;
	private Video video;
	
	public VideoCreation(JSONObject object) {
		parseObject(object);
	}

	//Parse json
	private void parseObject(JSONObject object) {
		upload = new VideoUpload(object.getJSONObject("upload"));
		video = new Video(object.getJSONObject("video"));
	}
	
	//Accessor Methods
	public VideoUpload getUpload() {
		return upload;
	}
	
	public Video getVideo() {
		return video;
	}
}
