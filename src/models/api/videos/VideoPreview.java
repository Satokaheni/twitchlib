package models.api.videos;

import org.json.JSONObject;

public class VideoPreview {

	private String large;
	private String medium;
	private String small;
	private String template;
	
	public VideoPreview(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		large = object.getString("large");
		medium = object.getString("medium");
		small = object.getString("small");
		template = object.getString("template");
	}

	//Accessor Methods
	public String getLarge() {
		return large;
	}
	
	public String getMedium() {
		return medium;
	}
	
	public String getSmall() {
		return small;
	}
	
	public String getTemplate() {
		return template;
	}
}
