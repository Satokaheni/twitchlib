package models.api.videos;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class Video {

	private String id;
	private String animated_preview_url;
	private String broadcast_id;
	private String broadcast_type;
	private VideoChannel channel;
	private DateTime created_at;
	private String description;
	private String description_html;
	private HashMap<String, Double> fps;
	private String game;
	private String language;
	private long length;
	private VideoMutedSegment[] muted_segments;
	private VideoPreview preview;
	private DateTime published_at;
	private DateTime recorded_at;
	private HashMap<String, String> resolutions;
	private String status;
	private String tag_list;
	private VideoThumbnails thumbnails;
	private String title;
	private String url;
	private String viewable;
	private DateTime viewable_at;
	private int views;
	
	public Video(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		animated_preview_url = object.getString("animated_preview_url");
		broadcast_id = object.getString("broadcast_id");
		broadcast_type = object.getString("broadcast_type");
		channel = new VideoChannel(object.getJSONObject("channel"));
		created_at = common.Parsing.convertToDateTime(object.getString("created_at"));
		description = object.getString("description");
		description_html = object.getString("description_html");
		
		Iterator<?> it = object.getJSONObject("fps").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			fps.put(key, object.getJSONObject("fps").getDouble(key));
		}
		
		game = object.getString("game");
		language = object.getString("language");
		length = object.getLong("length");
		
		if (object.has("muted_segment")) {
			muted_segments = new VideoMutedSegment[object.getJSONArray("muted_segments").length()];
			for (int i = 0; i < muted_segments.length; i++)
				muted_segments[i] = new VideoMutedSegment(object.getJSONArray("muted_segments").getJSONObject(i));
		}
		else
			muted_segments = null;
		
		preview = new VideoPreview(object.getJSONObject("preview"));
		published_at = common.Parsing.convertToDateTime(object.getString("published_at"));
		
		if (object.has("recorded_at"))
			recorded_at = common.Parsing.convertToDateTime(object.getString("recorded_at"));
		else
			recorded_at = null;
		
		it = object.getJSONObject("resolutions").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			resolutions.put(key, object.getJSONObject("resolutions").getString(key));
		}
		
		status = object.getString("status");
		tag_list = object.getString("tag_list");
		thumbnails = new VideoThumbnails(object.getJSONObject("thumbnails"));
		title = object.getString("title");
		url = object.getString("url");
		viewable = object.getString("viewable");
		viewable_at = common.Parsing.convertToDateTime(object.getString("viewable_at"));
		views = object.getInt("views");
	}
	
	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public String getAnimatedPreviewUrl() {
		return animated_preview_url;
	}
	
	public String getBroadcastId() {
		return broadcast_id;
	}
	
	public String getBroadcastType() {
		return broadcast_type;
	}
	
	public VideoChannel getChannel() {
		return channel;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getDescriptionHTML() {
		return description_html;
	}
	
	public HashMap<String, Double> getFPS() {
		return fps;
	}
	
	public String getGame() {
		return game;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public long getLength() {
		return length;
	}
	
	public VideoMutedSegment[] getMutedSegments() {
		return muted_segments;
	}
	
	public VideoPreview getPreview() {
		return preview;
	}
	
	public DateTime getPublishedAt() {
		return published_at;
	}
	
	public DateTime getRecordedAt() {
		return recorded_at;
	}
	
	public HashMap<String, String> getResolutions() {
		return resolutions;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getTagList() {
		return tag_list;
	}
	
	public VideoThumbnails getThumbnails() {
		return thumbnails;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getURL() {
		return url;
	}
	
	public String getViewable() {
		return viewable;
	}
	
	public DateTime getViewableAt() {
		return viewable_at;
	}
	
	public int getViews() {
		return views;
	}
}


