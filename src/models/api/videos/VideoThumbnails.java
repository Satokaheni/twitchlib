package models.api.videos;

import org.json.JSONObject;

public class VideoThumbnails {

	private VideoThumbnail[] large;
	private VideoThumbnail[] medium;
	private VideoThumbnail[] small;
	private VideoThumbnail[] template;
	
	public VideoThumbnails(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		large = new VideoThumbnail[object.getJSONArray("large").length()];
		for (int i = 0; i < large.length; i++)
			large[i] = new VideoThumbnail(object.getJSONArray("large").getJSONObject(i));
		
		medium = new VideoThumbnail[object.getJSONArray("medium").length()];
		for (int i = 0; i < medium.length; i++)
			medium[i] = new VideoThumbnail(object.getJSONArray("medium").getJSONObject(i));

		small = new VideoThumbnail[object.getJSONArray("small").length()];
		for (int i = 0; i < small.length; i++)
			small[i] = new VideoThumbnail(object.getJSONArray("small").getJSONObject(i));

		template = new VideoThumbnail[object.getJSONArray("template").length()];
		for (int i = 0; i < template.length; i++)
			template[i] = new VideoThumbnail(object.getJSONArray("template").getJSONObject(i));
	}
	
	//Accessor Methods
	public VideoThumbnail[] getLarge() {
		return large;
	}
	
	public VideoThumbnail[] getMedium() {
		return medium;
	}
	
	public VideoThumbnail[] getSmall() {
		return small;
	}

	public VideoThumbnail[] getTemplate() {
		return template;
	}
}
