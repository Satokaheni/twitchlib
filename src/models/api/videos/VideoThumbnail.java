package models.api.videos;

import org.json.JSONObject;

public class VideoThumbnail {

	private String type;
	private String url;
	
	public VideoThumbnail(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		type = object.getString("type");
		url = object.getString("url");
	}

	//Accessor Methods
	public String getType() {
		return type;
	}
	
	public String getURL() {
		return url;
	}
}
