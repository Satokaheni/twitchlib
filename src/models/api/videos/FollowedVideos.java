package models.api.videos;

import org.json.JSONObject;

public class FollowedVideos {
	
	private Video[] videos;
	
	public FollowedVideos(JSONObject object) {
		parseObject(object);
	}

	//Parse json
	private void parseObject(JSONObject object) {
		videos = new Video[object.getJSONArray("videos").length()];
		for (int i = 0; i < videos.length; i++)
			videos[i] = new Video(object.getJSONArray("videos").getJSONObject(i));
	}

	//Accessor Method
	public Video[] getVideos() {
		return videos;
	}
}
