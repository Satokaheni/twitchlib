package models.api.videos;

import org.json.JSONObject;

public class VideoMutedSegment {
	
	private long duration;
	private long offset;

	public VideoMutedSegment(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		duration = object.getLong("duration");
		offset = object.getLong("offset");
	}

	//Accessor Methods
	public long getDuration() {
		return duration;
	}
	
	public long getOffset() {
		return offset;
	}
}
