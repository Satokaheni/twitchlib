package models.api.videos;

import org.json.JSONObject;

public class VideoUpload {

	private String token;
	private String url;
	
	public VideoUpload(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		token = object.getString("token");
		url = object.getString("url");
	}

	
	//Accessor Methods
	public String getToken() {
		return token;
	}
	
	public String getURL() {
		return url;
	}
}
