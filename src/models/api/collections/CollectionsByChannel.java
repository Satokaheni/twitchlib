package models.api.collections;

import org.json.JSONObject;

public class CollectionsByChannel {

	private String cursor;
	private CollectionMetaData[] collections;
	
	public CollectionsByChannel(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		cursor = object.getString("_cursor");
		collections = new CollectionMetaData[object.getJSONArray("collections").length()];
		for (int i = 0; i < collections.length; i++)
			collections[i] = new CollectionMetaData(object.getJSONArray("collections").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getCursor() {
		return cursor;
	}

	public CollectionMetaData[] getCollections() {
		return collections;
	}
}
