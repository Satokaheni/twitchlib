package models.api.collections;

import org.json.JSONObject;

public class Collection {

	private String id;
	private CollectionItem[] items;
	
	public Collection(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		items = new CollectionItem[object.getJSONArray("items").length()];
		for (int i = 0; i < items.length; i++)
			items[i] = new CollectionItem(object.getJSONArray("items").getJSONObject(i));
	}

	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public CollectionItem[] getItems() {
		return items;
	}
}
