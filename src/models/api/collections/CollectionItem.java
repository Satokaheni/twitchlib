package models.api.collections;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class CollectionItem {

	private String id;
	private String description_html;
	private int duration;
	private String game;
	private String item_id;
	private String item_type;
	private User owner;
	private DateTime published_at;
	private HashMap<String, String> thumbnails = new HashMap<String, String>();
	private String title;
	private int views;
	
	public CollectionItem(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		description_html = object.getString("description_html");
		duration = object.getInt("duration");
		game = object.getString("game");
		item_id = object.getString("item_id");
		item_type = object.getString("item_type");
		owner = new User(object.getJSONObject("owner"));
		published_at = common.Parsing.convertToDateTime(object.getString("published_at"));
		
		Iterator<?> it = object.getJSONObject("thumbnails").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			thumbnails.put(key, object.getJSONObject("thumbnails").getString(key));
		}
		
		title = object.getString("title");
		views = object.getInt("views");
	}
	
	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public String getDescriptionHTML() {
		return description_html;
	}

	public int getDuration() {
		return duration;
	}
	
	public String getGame() {
		return game;
	}
	
	public String getItemId() {
		return item_id;
	}
	
	public String getItemType() {
		return item_type;
	}
	
	public User getOwner() {
		return owner;
	}
	
	public DateTime getPublishedAt() {
		return published_at;
	}
	
	public HashMap<String, String> getThumbnails() {
		return thumbnails;
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getViews() {
		return views;
	}
}
