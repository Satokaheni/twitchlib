package models.api.collections;

import java.util.HashMap;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class CollectionMetaData {

	private String id;
	private DateTime created_at;
	private int items_count;
	private User owner;
	private HashMap<String, String> thumbnails  = new HashMap<String, String>();
	private String title;
	private int total_duration;
	private DateTime updated_at;
	private int views;
	
	
	public CollectionMetaData(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		created_at = common.Parsing.convertToDateTime(object.getString("created_at"));
		items_count = object.getInt("items_count");
		owner = new User(object.getJSONObject("owner"));
		
		Iterator<?> it = object.getJSONObject("thumbnails").keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			thumbnails.put(key, object.getJSONObject("thumbnails").getString(key));
		}
		
		title = object.getString("title");
		total_duration = object.getInt("total_duration");
		updated_at = common.Parsing.convertToDateTime(object.getString("updated_at"));
		views = object.getInt("views");
	}
	
	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public int getItemsCount() {
		return items_count;
	}
	
	public User getOwner() {
		return owner;
	}
	
	public HashMap<String, String> getThumbnails() {
		return thumbnails;
	}
	
	public String getTitle() {
		return title;
	}

	public int getTotalDuration() {
		return total_duration;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public int getViews() {
		return views;
	}
}
