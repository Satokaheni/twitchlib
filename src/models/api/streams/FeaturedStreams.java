package models.api.streams;

import org.json.JSONObject;

public class FeaturedStreams {

	private FeaturedStream[] streams;
	
	public FeaturedStreams(JSONObject result) {
		parseResult(result);
	}

	//Parse Json
	private void parseResult(JSONObject result) {
		streams = new FeaturedStream[result.getJSONArray("featured").length()];
		for (int i = 0; i < streams.length; i++)
			streams[i] = new FeaturedStream(result.getJSONArray("featured").getJSONObject(i));
	}

	//Accessor Method
	public FeaturedStream[] getStreams() {
		return streams;
	}
}
