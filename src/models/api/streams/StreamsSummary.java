package models.api.streams;

import org.json.JSONObject;

public class StreamsSummary {

	private int channels;
	private int viewers;
	
	public StreamsSummary(JSONObject summary) {
		parseSummary(summary);
	}

	//parse json
	private void parseSummary(JSONObject summary) {
		channels = summary.getInt("channels");
		viewers = summary.getInt("viewers");
	}

	//Accessor Methods
	public int getChannels() {
		return channels;
	}
	
	public int getViewers() {
		return viewers;
	}
}
