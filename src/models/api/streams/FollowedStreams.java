package models.api.streams;

import org.json.JSONObject;

public class FollowedStreams {

	private int total;
	private Stream[] streams;
	
	public FollowedStreams(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		total = object.getInt("_total");
		streams = new Stream[object.getJSONArray("streams").length()];
		for (int i = 0; i < streams.length; i++)
			streams[i] = new Stream(object.getJSONArray("streams").getJSONObject(i));
	}
	
	//accessor methods
	public int getTotal() {
		return total;
	}
	
	public Stream[] getStreams() {
		return streams;
	}

}
