package models.api.streams;

import org.json.JSONObject;

public class FeaturedStream {

	private String image;
	private int priority;
	private boolean scheduled;
	private boolean sponsered;
	private Stream stream;
	private String text;
	private String title;
	
	public FeaturedStream(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		image = object.getString("image");
		priority = object.getInt("priority");
		scheduled = object.getBoolean("scheduled");
		sponsered = object.getBoolean("sponsered");
		stream = new Stream(object.getJSONObject("stream"));
		text = object.getString("text");
		title = object.getString("title");
	}
	
	//Accessor Methods
	public String getImage() {
		return image;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public boolean getScheduled() {
		return scheduled;
	}
	
	public boolean getSponsered() {
		return sponsered;
	}
	
	public Stream getStream() {
		return stream;
	}
	
	public String getText() {
		return text;
	}
	
	public String getTitle() {
		return title;
	}

}
