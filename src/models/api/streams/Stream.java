package models.api.streams;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.channels.Channel;

public class Stream {

	private int id;
	private double average_fps;
	private Channel channel;
	public DateTime created_at;
	private int delay;
	private String game;
	private boolean is_playlist;
	private StreamPreview preview;
	private int video_height;
	private int viewers;
	
	public Stream(JSONObject stream) {
		parseStream(stream);
	}

	//parse json
	private void parseStream(JSONObject stream) {
		id = stream.getInt("_id");
		average_fps = stream.getDouble("average_fps");
		channel = new Channel(stream.getJSONObject("channel"));
		created_at = common.Parsing.convertToDateTime(stream.getString("created_at"));
		delay = stream.getInt("delay");
		game = stream.getString("game");
		is_playlist = stream.getBoolean("is_playlist");
		preview = new StreamPreview(stream.getJSONObject("preview"));
		video_height = stream.getInt("video_height");
		viewers = stream.getInt("viewers");
	}
	
	//Accessor Method
	public int getId() {
		return id;
	}
	
	public double getAverageFPS() {
		return average_fps;
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public int getDelay() {
		return delay;
	}
	
	public String getGame() {
		return game;
	}
	
	public boolean getIsPlaylist() {
		return is_playlist;
	}
	
	public StreamPreview getPreview() {
		return preview;
	}
	
	public int getVideoHeight() {
		return video_height;
	}
	
	public int getViewers() {
		return viewers;
	}

}
