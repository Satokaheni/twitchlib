package models.api.streams;

import org.json.JSONObject;

public class LiveStreams {

	private int total;
	private Stream[] streams;
	
	public LiveStreams(JSONObject search) {
		parseSearch(search);
	}

	//parse Json
	private void parseSearch(JSONObject search) {
		total = search.getInt("_total");
		streams = new Stream[search.getJSONArray("streams").length()];
		for (int i = 0; i < streams.length; i++)
			streams[i] = new Stream(search.getJSONArray("streams").getJSONObject(i));
	}

	//accessor methods
	public int getTotal() {
		return total;
	}
	
	public Stream[] getStreams() {
		return streams;
	}
}
