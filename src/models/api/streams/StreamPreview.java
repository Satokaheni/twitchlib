package models.api.streams;

import org.json.JSONObject;

public class StreamPreview {

	private String large;
	private String medium;
	private String small;
	private String template;
	
	public StreamPreview(JSONObject preview) {
		parsePreview(preview);
	}

	//parse json
	private void parsePreview(JSONObject preview) {
		large = preview.getString("large");
		medium = preview.getString("medium");
		small = preview.getString("small");
		template = preview.getString("template");
	}

	//Accessor Method
	public String getLarge() {
		return large;
	}
	
	public String getMedium() {
		return medium;
	}
	
	public String getSmall() {
		return small;
	}
	
	public String getTemplate() {
		return template;
	}
}
