package models.api.clips;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class Clip {
	
	private String slug;
	private String tracking_id;
	private String url;
	private String embed_url;
	private String embed_html;
	private Broadcaster broadcaster;
	private Broadcaster curator;
	private VOD vod;
	private String game;
	private String language;
	private String title;
	private int views;
	private double duration;
	private DateTime created_at;
	private Thumbnail thumbnail;
	
	public Clip (JSONObject clip) {
		parseClip(clip);
	}

	//parse json
	private void parseClip(JSONObject clip) {
		slug = clip.getString("slug");
		tracking_id = clip.getString("tracking_id");
		url = clip.getString("url");
		embed_url = clip.getString("embed_url");
		embed_html = clip.getString("embed_html");
		broadcaster = new Broadcaster(clip.getJSONObject("broadcaster"));
		curator = new Broadcaster(clip.getJSONObject("curator"));
		vod = new VOD(clip.getJSONObject("vod"));
		game = clip.getString("game");
		language = clip.getString("language");
		title = clip.getString("title");
		views = clip.getInt("views");
		duration = clip.getDouble("duration");
		created_at = common.Parsing.convertToDateTime(clip.getString("created_at"));
		thumbnail = new Thumbnail(clip.getJSONObject("thumbnail"));
	}
	
	//accessor methods
	public String getSlug() {
		return slug;
	}
	
	public String getTrackingId() {
		return tracking_id;
	}
	
	public String getURL() {
		return url;
	}
	
	public String getEmbedURL() {
		return embed_url;
	}
	
	public String getEmbedHTML() {
		return embed_html;
	}
	
	public Broadcaster getBroadcaster() {
		return broadcaster;
	}
	
	public Broadcaster getCurator() {
		return curator;
	}
	
	public VOD getVOD() {
		return vod;
	}
	
	public String getGame() {
		return game;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getViews() {
		return views;
	}
	
	public double getDuration() {
		return duration;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public Thumbnail getThumbnail() {
		return thumbnail;
	}
}
