package models.api.clips;

import org.json.JSONObject;

public class Broadcaster {

	private String id;
	private String name;
	private String display_name;
	private String channel_url;
	private String logo;
	
	public Broadcaster(JSONObject broadcaster) {
		parseBroadcaster(broadcaster);
	}

	//Parse json
	private void parseBroadcaster(JSONObject broadcaster) {
		id = broadcaster.getString("id");
		name = broadcaster.getString("name");
		display_name = broadcaster.getString("display_name");
		channel_url = broadcaster.getString("channel_url");
		logo = broadcaster.getString("logo");
	}

	//Accessor methods
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getChannelURL() {
		return channel_url;
	}
	
	public String getLogo() {
		return logo;
	}
}
