package models.api.clips;

import org.json.JSONArray;

public class Clips {

	private Clip[] clips;
	
	public Clips(JSONArray c) {
		parseClips(c);
	}

	//parse json
	private void parseClips(JSONArray c) {
		clips = new Clip[c.length()];
		for (int i = 0; i < clips.length; i++)
			clips[i] = new Clip(c.getJSONObject(i));
	}
	
	//Accessor Method
	public Clip[] getClips() {
		return clips;
	}
}
