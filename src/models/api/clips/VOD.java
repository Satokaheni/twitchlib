package models.api.clips;

import org.json.JSONObject;

public class VOD {

	private String id;
	private String url;
	
	public VOD(JSONObject vod) {
		parseVOD(vod);
	}

	//parse json
	private void parseVOD(JSONObject vod) {
		id = vod.getString("id");
		url = vod.getString("url");
	}
	
	//Accessor methods
	public String getId() {
		return id;
	}
	
	public String getURL() {
		return url;
	}
}
