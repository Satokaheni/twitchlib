package models.api.clips;

import org.json.JSONObject;

public class Thumbnail {

	private String tiny;
	private String small;
	private String medium;
	
	public Thumbnail(JSONObject thumbnail) {
		parseThumbnail(thumbnail);
	}

	//parse json
	private void parseThumbnail(JSONObject thumbnail) {
		tiny = thumbnail.getString("tiny");
		small = thumbnail.getString("small");
		medium = thumbnail.getString("medium");
	}

	//Accessor Methods
	public String getTiny() {
		return tiny;
	}
	
	public String getSmall() {
		return small;
	}
	
	public String getMedium() {
		return medium;
	}
}
