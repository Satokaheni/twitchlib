package models.api.chat;

import org.json.JSONObject;

public class Emote {

	private String code;
	private int id;
	private int emoticon_set;
	
	public Emote(JSONObject emote) {
		parseEmote(emote);
	}
	
	public Emote(JSONObject emote, int set) {
		parseEmote(emote);
		emoticon_set = set;
	}

	//Parse Json
	private void parseEmote(JSONObject emote) {
		code = emote.getString("code");
		id = emote.getInt("id");
		if (emote.has("emoticon_set"))
			emoticon_set = emote.getInt("emoticon_set");
	}

	//Accessor Methods
	public String getCode() {
		return code;
	}
	
	public int getId() {
		return id;
	}
	
	public int getEmoticonSet() {
		return emoticon_set;
	}
}
