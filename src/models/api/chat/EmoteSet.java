package models.api.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONObject;

public class EmoteSet {

	private HashMap<String, Emote[]> emoticon_sets = new HashMap<String, Emote[]>();
	private Emote[] emoticons;
	
	public EmoteSet(JSONObject result) {
		parseEmoteSet(result);
	}

	//Parse Json
	private void parseEmoteSet(JSONObject result) {
		if (result.has("emoticon_sets")) {
			ArrayList<Emote> e = new ArrayList<Emote>();
			emoticon_sets = new HashMap<String, Emote[]>(); 
			Iterator<?> it = result.keys();
			while (it.hasNext()) {
				String key = (String) it.next();
				Emote[] emotes = new Emote[result.getJSONArray(key).length()];
				for (int i = 0; i < emotes.length; i++) {
					emotes[i] = new Emote(result.getJSONArray(key).getJSONObject(i), Integer.parseInt(key));
					e.add(emotes[i]);
				}
				
				emoticon_sets.put(key, emotes);
			}
			emoticons = (Emote[]) e.toArray();
		}
		else
			emoticon_sets = null;
		if (result.has("emoticons")) {
			HashMap<String, ArrayList<Emote>> set = new HashMap<String, ArrayList<Emote>>();
			emoticons = new Emote[result.getJSONArray("emoticons").length()];
			for (int i = 0; i < emoticons.length; i++) {
				emoticons[i] = new Emote(result.getJSONArray("emoticons").getJSONObject(i));
				String key = Integer.toString(emoticons[i].getEmoticonSet());
				if (set.containsKey(key)) {
					ArrayList<Emote> t = set.get(key);
					t.add(emoticons[i]);
					set.replace(key, t);
				}
				else {
					ArrayList<Emote> t = new ArrayList<Emote>();
					t.add(emoticons[i]);
					set.put(key, t);
				}
			}
			
			for (String key: set.keySet()) {
				emoticon_sets.put(key, (Emote[]) set.get(key).toArray());
			}
		}
	}
	
	//Accessor Methods
	public HashMap<String, Emote[]> getEmoticonSets() {
		return emoticon_sets;
	}
	
	public Emote[] getEmotes() {
		return emoticons;
	}

}
