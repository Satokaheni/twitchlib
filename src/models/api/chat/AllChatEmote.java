package models.api.chat;

import org.json.JSONObject;

public class AllChatEmote {

	private String regex;
	private EmoteImage[] images;
	
	public AllChatEmote(JSONObject emote) {
		parseEmote(emote);
	}

	//parse json
	private void parseEmote(JSONObject emote) {
		regex = emote.getString("regex");
		images = new EmoteImage[emote.getJSONArray("images").length()];
		for (int i = 0; i < images.length; i ++) 
			images[i] = new EmoteImage(emote.getJSONArray("images").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getRegex() {
		return regex;
	}
	
	public EmoteImage[] getImages() {
		return images;
	}

}
