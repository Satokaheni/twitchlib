package models.api.chat;

import org.json.JSONObject;

public class ChatBadges {

	private Badge admin;
	private Badge broadcaster;
	private Badge global_mod;
	private Badge mod;
	private Badge staff;
	private Badge subscriber;
	private Badge turbo;
	
	public ChatBadges(JSONObject badges) {
		parseBadges(badges);
	}

	//Check if value is null
	private Badge checkNull(JSONObject badge) {
		if (badge.equals(null))
			return null;
		return new Badge(badge);
	}
	
	//Parse Badges
	private void parseBadges(JSONObject badges) {
		admin = checkNull((JSONObject) badges.get("admin"));
		broadcaster = checkNull((JSONObject) badges.get("broadcasters"));
		global_mod = checkNull((JSONObject) badges.get("global_mod"));
		mod = checkNull((JSONObject) badges.get("mod"));
		staff = checkNull((JSONObject) badges.get("staff"));
		subscriber = checkNull((JSONObject) badges.get("subscriber"));
		turbo = checkNull((JSONObject) badges.get("turbo"));
	}
	
	//Accessor Methods
	public Badge getAdmin() {
		return admin;
	}
	
	public Badge getBroadcaster() {
		return broadcaster;
	}
	
	public Badge getGlobalMod() {
		return global_mod;
	}
	
	public Badge getMod() {
		return mod;
	}
	
	public Badge getStaff() {
		return staff;
	}
	
	public Badge getSubscriber() {
		return subscriber;
	}
	
	public Badge getTurbo() {
		return turbo;
	}

}
