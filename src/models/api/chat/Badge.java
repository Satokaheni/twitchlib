package models.api.chat;

import org.json.JSONObject;

public class Badge {

	private String alpha;
	private String image;
	private String svg;
	
	public Badge(JSONObject badge) {
		parseBadge(badge);
	}

	//Parse Badge
	private void parseBadge(JSONObject badge) {
		alpha = badge.getString("alpha");
		image = badge.getString("image");
		svg = badge.getString("svg");
	}
	
	//Accessor Methods
	public String getAlpha() {
		return alpha;
	}
	
	public String getImage() {
		return image;
	}
	
	public String getSVG() {
		return svg;
	}

}
