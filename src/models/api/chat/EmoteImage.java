package models.api.chat;

import org.json.JSONObject;

public class EmoteImage {

	private int width;
	private int height;
	private String url;
	private int emoticon_set;
	
	public EmoteImage(JSONObject image) {
		parseImage(image);
	}

	//Parse json
	private void parseImage(JSONObject image) {
		width = image.getInt("width");
		height = image.getInt("height");
		url = image.getString("url");
		emoticon_set = image.getInt("emoticon_set");
	}
	
	//Accessor Methods
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public String getURL() {
		return url;
	}
	
	public int getEmoticonSet() {
		return emoticon_set;
	}

}
