package models.api.chat;

import org.json.JSONArray;

public class AllChatEmotes {

	private AllChatEmote[] emotes;
	
	public AllChatEmotes(JSONArray allemotes) {
		parseEmotes(allemotes);
	}

	//parse json
	private void parseEmotes(JSONArray allemotes) {
		emotes = new AllChatEmote[allemotes.length()];
		for (int i = 0; i < emotes.length; i++)
			emotes[i] = new AllChatEmote(allemotes.getJSONObject(i));
	}
	
	//Accessor Method
	public AllChatEmote[] getAllEmotes() {
		return emotes;
	}

}
