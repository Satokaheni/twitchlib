package models.api.communities;

import org.json.JSONObject;

public class BannedUsers {
	
	private String cursor;
	private BannedUser[] banned_users;

	public BannedUsers(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		cursor = object.getString("_cursor");
		banned_users = new BannedUser[object.getJSONArray("banned_users").length()];
		for (int i = 0; i < banned_users.length; i++)
			banned_users[i] = new BannedUser(object.getJSONArray("banned_users").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getCursor() {
		return cursor;
	}

	public BannedUser[] getBannedUsers() {
		return banned_users;
	}
}
