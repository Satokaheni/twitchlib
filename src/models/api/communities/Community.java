package models.api.communities;

import org.json.JSONObject;

public class Community {

	private String id;
	private String avatar_image_url;
	private String cover_image_url;
	private String description;
	private String description_html;
	private String language;
	private String name;
	private String owner_id;
	private String rules;
	private String rules_html;
	private String summary;
	
	public Community(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		avatar_image_url = object.getString("avatar_image_url");
		cover_image_url = object.getString("cover_image_url");
		description = object.getString("description");
		description_html = object.getString("description_html");
		language = object.getString("language");
		name = object.getString("name");
		owner_id = object.getString("owner_id");
		rules = object.getString("rules");
		rules_html = object.getString("rules_html");
		summary = object.getString("summary");
	}
	
	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public String getAvatarImageURL() {
		return avatar_image_url;
	}
	
	public String getCoverImageURL() {
		return cover_image_url;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getDescriptionHTML() {
		return description_html;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public String getName() {
		return name;
	}
	
	public String getOwnerId() {
		return owner_id;
	}
	
	public String getRules() {
		return rules;
	}
	
	public String getRulesHTML() {
		return rules_html;
	}
	
	public String getSummary() {
		return summary;
	}
}
