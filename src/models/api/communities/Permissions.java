package models.api.communities;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONObject;

public class Permissions {

	private HashMap<String, Boolean> permissions = new HashMap<String, Boolean>();
	
	public Permissions(JSONObject object) {
		parseObject(object);
	}

	//parse Json
	private void parseObject(JSONObject object) {
		Iterator<?> it = object.keys();
		while(it.hasNext()) {
			String key = (String) it.next();
			permissions.put(key, object.getBoolean(key));
		}
	}
	
	//Accessor Method
	public HashMap<String, Boolean> getPermissions() {
		return permissions;
	}

}
