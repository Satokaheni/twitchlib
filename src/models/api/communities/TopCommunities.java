package models.api.communities;

import org.json.JSONObject;

public class TopCommunities {

	private String cursor;
	private int total;
	private TopCommunity[] communities;
	
	public TopCommunities(JSONObject object) {
		parseObject(object);
	}

	//Parse json
	private void parseObject(JSONObject object) {
		cursor = object.getString("_cursor");
		total = object.getInt("_total");
		communities = new TopCommunity[object.getJSONArray("communities").length()];
		for (int i = 0; i < communities.length; i++)
			communities[i] = new TopCommunity(object.getJSONArray("communities").getJSONObject(i));
	}

	//Accessor Methods
	public String getCursor() {
		return cursor;
	}
	
	public int getTotal() {
		return total;
	}
	
	public TopCommunity[] getCommunities() {
		return communities;
	}
}
