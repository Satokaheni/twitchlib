package models.api.communities;

import org.json.JSONObject;

public class BannedUser {

	private String user_id;
	private String display_name;
	private String name;
	private String bio;
	private String avatar_image_url;
	private long start_timestamp;
	
	public BannedUser(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		user_id = object.getString("user_id");
		display_name = object.getString("display_name");
		name = object.getString("name");
		bio = object.getString("bio");
		avatar_image_url = object.getString("avatar_image_url");
		start_timestamp = object.getLong("start_timestamp");
	}

	//Accessor Methods
	public String getUserId() {
		return user_id;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getName() {
		return name;
	}
	
	public String getBio() {
		return bio;
	}
	
	public String getAvatarImageURL() {
		return avatar_image_url;
	}
	
	public long getStartTimestamp() {
		return start_timestamp;
	}
}
