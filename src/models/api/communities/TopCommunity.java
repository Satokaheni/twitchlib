package models.api.communities;

import org.json.JSONObject;

public class TopCommunity {

	private String avatar_image_url;
	private int channels;
	private String id;
	private String name;
	private int viewers;
	
	public TopCommunity(JSONObject object) {
		parseObject(object);
	}

	//Parse json
	private void parseObject(JSONObject object) {
		avatar_image_url = object.getString("avatar_image_url");
		channels = object.getInt("channels");
		id = object.getString("id");
		name = object.getString("name");
		viewers = object.getInt("viewers");
	}

	//Accessor Methods
	public String getAvatarImageURL() {
		return avatar_image_url;
	}
	
	public int getChannels() {
		return channels;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getViewers() {
		return viewers;
	}
}
