package models.api.communities;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class Moderator {
	
	private String display_name;
	private String id;
	private String name;
	private String type;
	private String bio;
	private DateTime created_at;
	private DateTime updated_at;
	private String logo;

	public Moderator(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		display_name = object.getString("display_name");
		id = object.getString("_id");
		name = object.getString("name");
		type = object.getString("type");
		bio = object.getString("bio");
		created_at = common.Parsing.convertToDateTime(object.getString("created_at"));
		updated_at = common.Parsing.convertToDateTime(object.getString("updated_at"));
		logo = object.getString("logo");
	}
	
	//Accessor Methods
	public String getDisplayName() {
		return display_name;
	}
	
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public String getBio() {
		return bio;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public String getLogo() {
		return logo;
	}
}
