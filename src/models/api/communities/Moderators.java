package models.api.communities;

import org.json.JSONObject;

public class Moderators {

	private Moderator[] moderators;
	
	public Moderators(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		moderators = new Moderator[object.getJSONArray("moderators").length()];
		for (int i = 0; i < moderators.length; i++)
			moderators[i] = new Moderator(object.getJSONArray("moderators").getJSONObject(i));
	}

	//Accessor Method
	public Moderator[] getModerators() {
		return moderators;
	}
}
