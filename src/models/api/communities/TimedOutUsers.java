package models.api.communities;

import org.json.JSONObject;

public class TimedOutUsers {

	private String cursor;
	private TimedOutUser[] timed_out_users;
	
	public TimedOutUsers(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		cursor = object.getString("_cursor");
		timed_out_users = new TimedOutUser[object.getJSONArray("timed_out_users").length()];
		for (int i = 0; i < timed_out_users.length; i++)
			timed_out_users[i] = new TimedOutUser(object.getJSONArray("timed_out_users").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getCursor() {
		return cursor;
	}
	
	public TimedOutUser[] getTimedOutUsers() {
		return timed_out_users;
	}

}
