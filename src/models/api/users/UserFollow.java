package models.api.users;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.channels.Channel;


public class UserFollow {
	
	private DateTime created_at;
	private boolean notifications;
	private Channel channel;
	
	public UserFollow(JSONObject result) {
		parseResult(result);
	}

	//Parse json
	private void parseResult(JSONObject result) {
		created_at = common.Parsing.convertToDateTime(result.getString("created_at"));
		notifications = result.getBoolean("notifications");
		channel = new Channel((JSONObject) result.get("channel"));
	}
	
	//Accessor methods
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public boolean getNotifications() {
		return notifications;
	}
	
	public Channel getChannel() {
		return channel;
	}
}
