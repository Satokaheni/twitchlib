package models.api.users;

import java.util.HashMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONObject;

public class User {
	
	private int id;
	private String bio = null;
	private DateTime created_at;
	private String display_name;
	private String logo = null;
	private String name;
	private String type;
	private DateTime updated_at;
	
	public User(JSONObject result) {
		getParams(result);
	}
	
	//Get values for parameters
	private void getParams(JSONObject result) {
		id = result.getInt("_id");
		if (result.has("bio"))
			bio = result.getString("bio");
		created_at = common.Parsing.convertToDateTime(result.getString("created_at"));
		display_name = result.getString("display_name");
		if (result.has("logo"))
			logo = result.getString("logo");
		name = result.getString("name");
		type = result.getString("type");
		updated_at = common.Parsing.convertToDateTime(result.getString("updated_at"));
	}
	
	
	//Accessor Methods
	public int getId() {
		return id;
	}
	
	public String getBio() {
		return bio;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getLogo() {
		return logo;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
}
