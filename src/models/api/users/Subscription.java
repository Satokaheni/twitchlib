package models.api.users;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.channels.Channel;

public class Subscription {

	private String id;
	private String sub_plan;
	private String sub_plan_name;
	private Channel channel;
	private DateTime created_at;
	
	public Subscription(JSONObject result) {
		parseResult(result);
	}
	
	//parse JSON
	private void parseResult(JSONObject result) {
		id = result.getString("_id");
		sub_plan = result.getString("sub_plan");
		sub_plan_name = result.getString("sub_plan_name");
		channel = new Channel(result.getJSONObject("channel"));
		created_at = common.Parsing.convertToDateTime(result.getString("created_at"));
	}
	
	//Accessor methods
	public String getId() {
		return id;
	}
	
	public String getSubPlan() {
		return sub_plan;
	}
	
	public String getSubPlanName() {
		return sub_plan_name;
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}

}
