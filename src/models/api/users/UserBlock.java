package models.api.users;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class UserBlock {
	private int id;
	private DateTime updated_at;
	private User user;
	
	public UserBlock(JSONObject result) {
		parseBlock(result);
	}

	//Parse Json
	private void parseBlock(JSONObject result) {
		id = result.getInt("_id");
		updated_at = common.Parsing.convertToDateTime(result.getString("updated_at"));
		user = new User((JSONObject) result.get("user"));
	}
	
	//Accessor methods
	public int getId() {
		return id;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public User getUser() {
		return user;
	}

}
