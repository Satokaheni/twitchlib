package models.api.users;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

public class EmoteSet {
	
	private HashMap<String, Emote[]> emote_sets = new HashMap<String, Emote[]>(); 
	
	public EmoteSet(JSONObject result) {
		emote_sets = new HashMap<String, Emote[]>();
		parseResult(result);
	}
	
	//parse json
	private void parseResult(JSONObject result) {
		Iterator<?> it = result.keys();
		
		while (it.hasNext()) {
			String key = (String)it.next();
			JSONArray emotes = result.getJSONArray(key);
			Emote[] e = new Emote[emotes.length()];
			for (int i = 0; i < emotes.length(); i ++) {
				e[i] = new Emote((JSONObject) emotes.get(i));
				
			}
			emote_sets.put(key, e);
		}
	}
	
	//accessor method
	public HashMap<String, Emote[]> getEmoteSets() {
		return emote_sets;
	}
}
