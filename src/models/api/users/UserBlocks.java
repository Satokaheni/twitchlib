package models.api.users;

import org.json.JSONArray;
import org.json.JSONObject;

public class UserBlocks {

	UserBlock[] user_blocks;
	
	public UserBlocks(JSONArray result) {
		parseList(result);
	}

	//Parse json list
	private void parseList(JSONArray result) {
		user_blocks = new UserBlock[result.length()];
		for (int i = 0; i < result.length(); i++)
			user_blocks[i] = new UserBlock((JSONObject) result.get(i));
	}
	
	public UserBlock[] getUserBlockList() {
		return user_blocks;
	}

}
