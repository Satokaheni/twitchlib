package models.api.users;

import org.json.JSONArray;
import org.json.JSONObject;

public class UserFollows {

	private UserFollow[] userfollows;
	
	public UserFollows(JSONArray result) {
		userfollows = new UserFollow[result.length()];
		parseFollowers(result);
	}
	
	//get all followers in array
	private void parseFollowers(JSONArray result) {
		for (int i = 0; i < result.length(); i++)
			userfollows[i] = new UserFollow((JSONObject) result.get(i));
	}
	
	//Accessor Method
	public UserFollow[] getFollowers() {
		return userfollows;
	}
	
}
