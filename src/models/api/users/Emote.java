package models.api.users;

import org.json.JSONObject;

public class Emote {

	private String code;
	private int id;
	
	public Emote(JSONObject emote) {
		parseEmote(emote);
	}

	//parse json
	private void parseEmote(JSONObject emote) {
		code = emote.getString("code");
		id = emote.getInt("id");
	}
	
	//Accesor methods
	public String getCode() {
		return code;
	}
	
	public int getId() {
		return id;
	}
	
}
