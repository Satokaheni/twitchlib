package models.api.users;

import org.json.JSONArray;
import org.json.JSONObject;

public class Users {

	User[] users;
	
	public Users(JSONArray result) {
		users = new User[result.length()];
		getAllUsers(result);
	}
	
	//get all users from request
	private void getAllUsers(JSONArray result) {
		for(int i = 0; i < users.length; i++)
			users[i] = new User((JSONObject) result.get(i));
	}
	
	//Accessor method
	public User[] getUsers() {
		return users;
	}
}
