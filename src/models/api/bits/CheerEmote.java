package models.api.bits;

import org.json.JSONArray;
import org.json.JSONObject;

public class CheerEmote {

	private Action[] actions;
	
	public CheerEmote(JSONArray result) {
		parseEmotes(result);
	}

	//Parse Emotes
	private void parseEmotes(JSONArray result) {
		actions = new Action[result.length()];
		for (int i = 0; i < result.length(); i++)
			actions[i] = new Action((JSONObject) result.get(i));
	}
	
	//Accessor Method
	public Action[] getActions(){
		return actions;
	}

}
