package models.api.bits;

import org.json.JSONObject;

public class DarkImage {

	private ImageLinks animated_links;
	private ImageLinks static_links;
	
	public DarkImage(JSONObject darkimage) {
		parseDarkImage(darkimage);
	}

	//parse DarkImage
	private void parseDarkImage(JSONObject darkimage) {
		animated_links = new ImageLinks((JSONObject) darkimage.get("animated"));
		static_links = new ImageLinks((JSONObject) darkimage.get("static"));
	}
	
	//Accessor Methods
	public ImageLinks getAnimated() {
		return animated_links;
	}
	
	public ImageLinks getStatic() {
		return static_links;
	}

}
