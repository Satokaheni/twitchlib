package models.api.bits;

import org.json.JSONObject;

public class ImageLinks {

	private String one;
	private String one_five;
	private String two;
	private String three;
	private String four;
	
	public ImageLinks(JSONObject links) {
		parseLinks(links);
	}

	//Parse Links
	private void parseLinks(JSONObject links) {
		one = links.getString("1");
		one_five = links.getString("1.5");
		two = links.getString("2");
		three = links.getString("3");
		four = links.getString("4");
	}
	
	//Accessor Methods
	public String getOne() {
		return one;
	}
	
	public String getOnePointFive(){
		return one_five;
	}
	
	public String getTwo() {
		return two;
	}
	
	public String getThree() {
		return three;
	}
	
	public String getFour() {
		return four;
	}
}
