package models.api.bits;

import org.json.JSONObject;

public class Tier {
	private int min_bits;
	private String id;
	private String color;
	private Image[] images;
	
	public Tier(JSONObject tier) {
		parseTier(tier);
	}

	//Parse Tier
	private void parseTier(JSONObject tier) {
		min_bits = tier.getInt("min_bits");
		id = tier.getString("id");
		color = tier.getString("color");
		images = new Image[tier.getJSONArray("images").length()];
		for (int i = 0; i < images.length; i++)
			images[i] = new Image((JSONObject) tier.getJSONArray("images").get(i));
	}

	//Accessor Methods
	public int getMinBits() {
		return min_bits;
	}
	
	public String getId() {
		return id;
	}
	
	public String getColor() {
		return color;
	}
	
	public Image[] getImages() {
		return images;
	}
}
