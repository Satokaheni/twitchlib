package models.api.bits;

import org.json.JSONObject;

public class Image {

	private DarkImage image_dark;
	private LightImage image_light;
	
	public Image(JSONObject image) {
		parseImage(image);
	}

	//Parse Image
	private void parseImage(JSONObject image) {
		image_dark = new DarkImage((JSONObject) image.get("dark"));
		image_light = new LightImage((JSONObject) image.get("light"));
	}
	
	//Accessor Methods
	public DarkImage getDarkImage() {
		return image_dark;
	}
	
	public LightImage getLightImage() {
		return image_light;
	}

}
