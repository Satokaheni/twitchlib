package models.api.bits;

import org.json.JSONObject;

public class LightImage {

	private ImageLinks animated_links;
	private ImageLinks static_links;
	
	public LightImage(JSONObject lightimage) {
		parseLightImage(lightimage);
	}

	//parse DarkImage
	private void parseLightImage(JSONObject lightimage) {
		animated_links = new ImageLinks((JSONObject) lightimage.get("animated"));
		static_links = new ImageLinks((JSONObject) lightimage.get("static"));
	}
	
	//Accessor Methods
	public ImageLinks getAnimated() {
		return animated_links;
	}
	
	public ImageLinks getStatic() {
		return static_links;
	}

}
