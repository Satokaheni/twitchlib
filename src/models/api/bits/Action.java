package models.api.bits;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class Action {

	private String prefix;
	private String[] scales;
	private Tier[] tiers;
	private String[] backgrounds;
	private String[] states;
	private String type;
	private DateTime updated_at;
	private int priority;
	
	public Action(JSONObject action) {
		parseAction(action);
	}

	//Parse Json
	private void parseAction(JSONObject action) {
		prefix = action.getString("prefix");
		scales = new String[action.getJSONArray("scales").length()];
		for (int i = 0; i < scales.length; i++)
			scales[i] = action.getJSONArray("scales").getString(i);
		
		tiers = new Tier[action.getJSONArray("tiers").length()];
		for (int i = 0; i < tiers.length; i++)
			tiers[i] = new Tier((JSONObject) action.getJSONArray("tiers").get(i));
		
		backgrounds = new String[action.getJSONArray("backgrounds").length()];
		for (int i = 0; i < backgrounds.length; i++)
			backgrounds[i] = action.getJSONArray("backgrounds").getString(i);
		
		states = new String[action.getJSONArray("states").length()];
		for (int i = 0; i < backgrounds.length; i++)
			states[i] = action.getJSONArray("states").getString(i);
		
		type = action.getString("type");
		
		updated_at = common.Parsing.convertToDateTime(action.getString("updated_at"));
		priority = action.getInt("priority");
	}

	//Accessor Methods
	public String getPrefix() {
		return prefix;
	}
	
	public String[] getScales() {
		return scales;
	}
	
	public Tier[] getTiers() {
		return tiers;
	}
	
	public String[] getBackgrounds() {
		return backgrounds;
	}
	
	public String[] getStates() {
		return states;
	}
	
	public String getType() {
		return type;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public int getPriority() {
		return priority;
	}
}
