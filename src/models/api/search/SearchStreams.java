package models.api.search;

import org.json.JSONObject;

import models.api.streams.Stream;

public class SearchStreams {

	private int total;
	private Stream[] streams;
	
	public SearchStreams(JSONObject search) {
		parseSearch(search);
	}

	//parse json
	private void parseSearch(JSONObject search) {
		total = search.getInt("_total");
		streams = new Stream[search.getJSONArray("streams").length()];
		for (int i = 0; i < streams.length; i++)
			streams[i] = new Stream(search.getJSONArray("streams").getJSONObject(i));
	}
	
	//Accessor Methods
	public int getTotal() {
		return total;
	}
	
	public Stream[] getStreams() {
		return streams;
	}

}
