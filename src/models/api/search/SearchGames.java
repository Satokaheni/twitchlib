package models.api.search;

import org.json.JSONObject;

import models.api.games.Game;

public class SearchGames {

	private Game[] games;
	
	public SearchGames(JSONObject search) {
		parseSearch(search);
	}

	//parse json
	private void parseSearch(JSONObject search) {
		games = new Game[search.getJSONArray("games").length()];
		for (int i = 0; i < games.length; i++)
			games[i] = new Game(search.getJSONArray("games").getJSONObject(i));
	}
	
	//Accessor Methods
	public Game[] getGames() {
		return games;
	}

}
