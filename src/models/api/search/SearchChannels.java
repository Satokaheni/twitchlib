package models.api.search;

import org.json.JSONObject;

import models.api.channels.Channel;

public class SearchChannels {

	private int total;
	private Channel[] channels;
	
	public SearchChannels(JSONObject search) {
		parseSearch(search);
	}

	//parse json
	private void parseSearch(JSONObject search) {
		total = search.getInt("_total");
		channels = new Channel[search.getJSONArray("channels").length()];
		for (int i = 0; i < channels.length; i++) 
			channels[i] = new Channel(search.getJSONArray("channels").optJSONObject(i));
	}
	
	//Accessor Methods
	public int getTotal() {
		return total;
	}
	
	public Channel[] getChannels() {
		return channels;
	}

}
