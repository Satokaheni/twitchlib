package models.api.games;

import org.json.JSONObject;

public class Box {

	private String large;
	private String medium;
	private String small;
	private String template;
	
	public Box(JSONObject box) {
		parseBox(box);
	}

	//Parse Json
	private void parseBox(JSONObject box) {
		large = box.getString("large");
		medium = box.getString("medium");
		small = box.getString("small");
		template = box.getString("template");
	}

	//Accessor Methods
	public String getLarge() {
		return large;
	}
	
	public String getMedium() {
		return medium;
	}
	
	public String getSmall() {
		return small;
	}
	
	public String getTemplate() {
		return template;
	}
}
