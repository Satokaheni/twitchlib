package models.api.games;

import org.json.JSONObject;

public class Games {

	private int total;
	private Game[] games;
	
	public Games(JSONObject gamesList) {
		parseGames(gamesList);
	}

	//parse Json
	private void parseGames(JSONObject gamesList) {
		total = gamesList.getInt("_total");
		games = new Game[gamesList.getJSONArray("top").length()];
		for (int i = 0; i < games.length; i++)
			games[i] = new Game(gamesList.getJSONArray("top").getJSONObject(i));
	}
	
	//Accessor Methods
	public int getTotal() {
		return total;
	}
	
	public Game[] getGames() {
		return games;
	}

}
