package models.api.games;

import org.json.JSONObject;

public class Logo {

	private String large;
	private String medium;
	private String small;
	private String template;
	
	public Logo(JSONObject logo) {
		parseLogo(logo);
	}

	//Parse Json
	private void parseLogo(JSONObject logo) {
		large = logo.getString("large");
		medium = logo.getString("medium");
		small = logo.getString("small");
		template = logo.getString("template");
	}

	//Accessor Methods
	public String getLarge() {
		return large;
	}
	
	public String getMedium() {
		return medium;
	}
	
	public String getSmall() {
		return small;
	}
	
	public String getTemplate() {
		return template;
	}

}
