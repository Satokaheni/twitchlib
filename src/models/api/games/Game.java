package models.api.games;

import org.json.JSONObject;

public class Game {

	private int channels;
	private int viewers;
	private int id;
	private Box box;
	private int giantbomb_id;
	private Logo logo;
	private String name;
	private int popularity;
	
	public Game(JSONObject game) {
		parseGame(game);
	}
	
	//Parse Json
	private void parseGame(JSONObject game) {
		channels = game.getInt("channels");
		viewers = game.getInt("viewers");
		id = game.getJSONObject("game").getInt("_id");
		box = new Box(game.getJSONObject("game").getJSONObject("box"));
		giantbomb_id = game.getInt("giantbomb_id");
		logo = new Logo(game.getJSONObject("logo"));
		name = game.getString("game");
		popularity = game.getInt("popularity");
	}

	//Accessor Methods
	public int getChannels() {
		return channels;
	}
	
	public int viewers() {
		return viewers;
	}
	
	public int getId() {
		return id;
	}
	
	public Box getBox() {
		return box;
	}
	
	public int getGiantbombId() {
		return giantbomb_id;
	}
	
	public Logo getLogo() {
		return logo;
	}
	
	public String getName() {
		return name;
	}
	
	public int getPopularity() {
		return popularity;
	}
}
