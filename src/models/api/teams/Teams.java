package models.api.teams;

import org.json.JSONArray;

public class Teams {

	private Team[] teams;
	
	public Teams(JSONArray t) {
		parseTeams(t);
	}

	//Parse Json
	private void parseTeams(JSONArray t) {
		teams = new Team[t.length()];
		for (int i = 0; i < teams.length; i++)
			teams[i] = new Team(t.getJSONObject(i));
	}
	
	//Accessor Method
	public Team[] getTeams() {
		return teams;
	}

}
