package models.api.teams;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.channels.Channel;

public class Team {

	private int id;
	private String background;
	private String banner;
	private DateTime created_at;
	private String display_name;
	private String info;
	private String logo;
	private String name;
	private DateTime updated_at;
	private Channel[] users;
	
	public Team(JSONObject team) {
		parseTeam(team);
	}

	//parse json
	private void parseTeam(JSONObject team) {
		id = team.getInt("_id");
		background = team.getString("background");
		banner = team.getString("banner");
		created_at = common.Parsing.convertToDateTime(team.getString("created_at"));
		display_name = team.getString("display_name");
		info = team.getString("info");
		logo = team.getString("logo");
		name = team.getString("name");
		updated_at = common.Parsing.convertToDateTime(team.getString("updated_at"));
		if (team.has("users")) {
			users = new Channel[team.getJSONArray("user").length()];
			for (int i = 0; i < users.length; i++)
				users[i] = new Channel(team.getJSONArray("users").getJSONObject(i));
		}
		else
			users = null;
	}
	
	//Accessor Methods
	public int getId() {
		return id;
	}
	
	public String getBackground() {
		return background;
	}
	
	public String getBanner() {
		return banner;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getInfo() {
		return info;
	}
	
	public String getLogo() {
		return logo;
	}
	
	public String getName() {
		return name;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}

	public Channel[] getUsers() {
		return users;
	}
}
