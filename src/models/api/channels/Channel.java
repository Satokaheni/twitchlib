package models.api.channels;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class Channel {
	private int id;
	private String background = null;
	private String banner = null;
	private String broadcaster_language;
	private DateTime created_at;
	private String delay = null;
	private String display_name;
	private int followers;
	private String game = null;
	private String language;
	private String logo = null;
	private boolean mature;
	private String name;
	private boolean partner;
	private String profile_banner = null;
	private String profile_banner_background_color = null;
	private String status = null;
	private DateTime updated_at;
	private String url;
	private String video_banner = null;
	private int views;
	
	public Channel(JSONObject result) {
		parseReult(result);
	}

	private void parseReult(JSONObject result) {
		id = result.getInt("_id");
		if (result.has("background"))
			background = result.getString("background");
		if (result.has("banner"))
			banner = result.getString("banner");
		broadcaster_language = result.getString("broadcaster_language");
		created_at = common.Parsing.convertToDateTime(result.getString("created_at"));
		if (result.has("delay"))
			delay = result.getString("delay");
		display_name = result.getString("display_name");
		followers = result.getInt("followers");
		if (result.has("game"))
			game = result.getString("game");
		language = result.getString("language");
		if (result.has("logo"))
			logo = result.getString("logo");
		mature = result.getBoolean("mature");
		name = result.getString("name");
		partner = result.getBoolean("partner");
		if (result.has("profile_banner"))
			profile_banner = result.getString("profile_banner");
		if (result.has("profile_banner_background_color"))
			profile_banner_background_color = result.getString("profile_banner_background_color");
		if (result.has("status"))
			status = result.getString("status");
		updated_at = common.Parsing.convertToDateTime(result.getString("updated_at"));
		if (result.has("video_banner"))
			video_banner = result.getString("video_banner");
		views = result.getInt("views");
	}
	
	//Acessor Method
	public int getId() {
		return id;
	}
	
	public String getBackground() {
		return background;
	}
	
	public String getBanner() {
		return banner;
	}
	
	public String getBroadcasterLanguage() {
		return broadcaster_language;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}

	public String getDelay() {
		return delay;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public int getFollowers() {
		return followers;
	}
	
	public String getGame() {
		return game;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public String getLogo() {
		return logo;
	}
	
	public boolean getMature() {
		return mature;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getPartner() {
		return partner;
	}
	
	public String getProfileBanner() {
		return profile_banner;
	}
	
	public String getProfileBannerBackgroundColor() {
		return profile_banner_background_color;
	}
	
	public String getStatus() {
		return status;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public String getURL() {
		return url;
	}
	
	public String getVideoBanner() {
		return video_banner;
	}
	
	public int getViews() {
		return views;
	}
}

