package models.api.channels;

import org.json.JSONArray;

import models.api.teams.Team;

public class ChannelTeams {

	private Team[] teams;
	
	public ChannelTeams(JSONArray array) {
		parseArray(array);
	}

	//Parse Json
	private void parseArray(JSONArray array) {
		teams = new Team[array.length()];
		for (int i = 0; i < teams.length; i++)
			teams[i] = new Team(array.getJSONObject(i));
	}

	//Accessor Method
	public Team[] getTeams() {
		return teams;
	}
}
