package models.api.channels;

import org.json.JSONArray;

import models.api.users.User;

public class ChannelEditors {

	public User[] users;
	
	public ChannelEditors(JSONArray jsonArray) {
		parseArray(jsonArray);
	}

	//Parse Json
	private void parseArray(JSONArray jsonArray) {
		users = new User[jsonArray.length()];
		for (int i = 0; i < users.length; i++)
			users[i] = new User(jsonArray.getJSONObject(i));
	}

	//Accessor Method
	public User[] getUsers() {
		return users;
	}
}
