package models.api.channels;

import org.json.JSONObject;

import models.api.users.Subscription;

public class ChannelSubscribers {

	private int total;
	private Subscription[] subscribers; 
	
	public ChannelSubscribers(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		total = object.getInt("_total");
		subscribers = new Subscription[object.getJSONArray("subscriptions").length()];
		for (int i = 0; i < subscribers.length; i++)
			subscribers[i] = new Subscription(object.getJSONArray("subscriptions").getJSONObject(i));
	}

	//Accessor Methods
	public int getTotal() {
		return total;
	}
	
	public Subscription[] getSubscribers() {
		return subscribers;
	}
}
