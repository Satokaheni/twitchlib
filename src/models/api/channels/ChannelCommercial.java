package models.api.channels;

import org.json.JSONObject;

public class ChannelCommercial {

	private int length;
	private String message;
	private int retryafter;
	
	public ChannelCommercial(JSONObject object) {
		parseObject(object);
	}

	//parse json
	private void parseObject(JSONObject object) {
		length = object.getInt("Length");
		message = object.getString("Message");
		retryafter = object.getInt("RetryAfter");
	}

	//Accessor Methods
	public int getLength() {
		return length;
	}
	
	public String getMessage() {
		return message;
	}
	
	public int getRetryAfter() {
		return retryafter;
	}
}
