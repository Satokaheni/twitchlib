package models.api.channels;

import org.joda.time.DateTime;
import org.json.JSONObject;

import models.api.users.User;

public class ChannelFollow {

	private DateTime created_at;
	private boolean notifications;
	private User user;
	
	public ChannelFollow(JSONObject object) {
		parseObject(object);
	}

	//parse Json
	private void parseObject(JSONObject object) {
		created_at = common.Parsing.convertToDateTime(object.getString("created_at"));
		notifications = object.getBoolean("notifications");
		user = new User(object.getJSONObject("user"));
	}

	//Accessor Methods
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public boolean getNotifications() {
		return notifications;
	}
	
	public User getUser() {
		return user;
	}
}
