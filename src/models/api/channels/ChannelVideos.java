package models.api.channels;

import org.json.JSONObject;

import models.api.videos.Video;

public class ChannelVideos {

	private int total;
	private Video[] videos;
	
	public ChannelVideos(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		total = object.getInt("_total");
		videos = new Video[object.getJSONArray("videos").length()];
		for (int i = 0; i < videos.length; i++)
			videos[i] = new Video(object.getJSONArray("videos").getJSONObject(i));
	}
	
	//Accessor Methods
	public int getTotal() {
		return total;
	}
	
	public Video[] getVideos() {
		return videos;
	}
}
