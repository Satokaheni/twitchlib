package models.api.channels;

import org.json.JSONObject;

public class ChannelFollowers {

	private String cursor;
	private int total;
	private ChannelFollow[] follows;
	
	public ChannelFollowers(JSONObject object) {
		parseObject(object);
	}

	//parse Json
	private void parseObject(JSONObject object) {
		cursor = object.getString("_cursor");
		total = object.getInt("_total");
		follows = new ChannelFollow[object.getJSONArray("follows").length()];
		for (int i = 0; i < follows.length; i++)
			follows[i] = new ChannelFollow(object.getJSONArray("follows").getJSONObject(i));
	}
	
	//Accessor Methods
	public String getCursor() {
		return cursor;
	}

	public int getTotal() {
		return total;
	}
	
	public ChannelFollow[] getFollows() {
		return follows;
	}
}
