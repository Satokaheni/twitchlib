package models.api.channels;

import org.joda.time.DateTime;
import org.json.JSONObject;

public class ChannelAuthed {

	private String id;
	private String broadcaster_type;
	private String broadcaster_language;
	private DateTime created_at;
	private String display_name;
	private String email;
	private int followers;
	private String game;
	private String language;
	private String logo;
	private boolean mature;
	private String name;
	private boolean partner;
	private String profile_banner;
	private String profile_banner_background_color;
	private String status;
	private String stream_key;
	private DateTime updated_at;
	private String url;
	private String video_banner;
	private int views;
	
	public ChannelAuthed(JSONObject object) {
		parseObject(object);
	}

	//Parse Json
	private void parseObject(JSONObject object) {
		id = object.getString("_id");
		broadcaster_type = object.getString("broadcaster_type");
		broadcaster_language = object.getString("broadcaster_language");
		created_at = common.Parsing.convertToDateTime(object.getString("created_at"));
		display_name = object.getString("display_name");
		email = object.getString("email");
		followers = object.getInt("followers");
		game = object.getString("game");
		language = object.getString("language");
		logo = object.getString("logo");
		mature = object.getBoolean("mature");
		name = object.getString("name");
		partner = object.getBoolean("partner");
		profile_banner = object.getString("profile_banner");
		profile_banner_background_color = object.getString("profile_banner_background_color");
		status = object.getString("status");
		stream_key = object.getString("stream_key");
		updated_at = common.Parsing.convertToDateTime(object.getString("updated_at"));
		url = object.getString("url");
		video_banner = object.getString("video_banner");
		views = object.getInt("views");
	}

	//Accessor Methods
	public String getId() {
		return id;
	}
	
	public String getBroadcasterType() {
		return broadcaster_type;
	}
	
	public String getBroadcasterLanguage() {
		return broadcaster_language;
	}
	
	public DateTime getCreatedAt() {
		return created_at;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public int getFollowers() {
		return followers;
	}
	
	public String getGame() {
		return game;
	}
	
	public String getLanguage() {
		return language;
	}
	
	public String getLogo() {
		return logo;
	}
	
	public boolean getMature() {
		return mature;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getPartner() {
		return partner;
	}
	
	public String getProfileBanner() {
		return profile_banner;
	}
	
	public String getProfileBannerBackgroundColor() {
		return profile_banner_background_color;
	}
	
	public String getStatus() {
		return status;
	}
	
	public String getStreamKey() {
		return stream_key;
	}
	
	public DateTime getUpdatedAt() {
		return updated_at;
	}
	
	public String getURL() {
		return url;
	}
	
	public String getVideoBanner() {
		return video_banner;
	}
	
	public int getViews() {
		return views;
	}
}

