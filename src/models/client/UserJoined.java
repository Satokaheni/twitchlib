package models.client;

public class UserJoined {

	public String username;
	public String channel;
	
	public UserJoined(String username, String channel) {
		this.username = username;
		this.channel = channel;
	}
}
