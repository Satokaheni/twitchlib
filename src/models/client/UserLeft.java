package models.client;

public class UserLeft {

	public String username;
	public String channel;
	
	public UserLeft(String username, String channel) {
		this.username = username;
		this.channel = channel;
	}
}
