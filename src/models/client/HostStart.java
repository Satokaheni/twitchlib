package models.client;

public class HostStart {

	private String hosting_channel;
	private String target_channel;
	private int viewers = -1;
	
	public HostStart(String message) {
		hosting_channel = message.split(" ")[2].replaceAll("#", "").toLowerCase();
		target_channel = message.split(" ")[3].toLowerCase();
		if (message.split(" ").length > 4)
			viewers = Integer.parseInt(message.split(" ")[4]);
	}
	
	//Accessor Methods
	public String getHostChannel() {
		return hosting_channel;
	}
	
	public String getTargetChannel() {
		return target_channel;
	}
	
	public int getViewers() {
		return viewers;
	}
}
