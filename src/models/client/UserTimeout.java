package models.client;

public class UserTimeout {

	public String username;
	public String channel;
	public int duration;
	public String reason;
	
	public UserTimeout(String message) {
		username = message.split(":")[1].toLowerCase();
		channel = message.split("CLEARCHAT #")[1].split(" ")[0].toLowerCase();
		duration = Integer.parseInt(message.split(";")[0].split("=")[1]);
		reason = message.split(";")[1].split(" ")[0].split("=")[1].toLowerCase();
	}
}
