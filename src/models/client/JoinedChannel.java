package models.client;

public class JoinedChannel {
	
	private ChannelState channelState = null;
	private String channel;
	private ChatMessage previousMessage = null;
	
	public JoinedChannel(String channel) {
		this.channel = channel;
	}
	
	//Accessor Methods
	public String getChannel() {
		return channel;
	}
	
	public ChannelState getChannelState() {
		return channelState;
	}
	
	public ChatMessage getPreviousMessage() {
		return previousMessage;
	}
	
	//setter method
	public void handleMessage(ChatMessage message) {
		previousMessage = message;
	}
	
}
