package models.client;

public class ChatMessage {
	private String BotUsername;
	private String UserId;
	private String Username;
	private String Message;
	private String Channel;
	private boolean isSub = false;
	private boolean isBroadcaster = false;
	private int SubMonthCount = 0;
	private String RoomId;
	private boolean isMod = false;
	private boolean isTurbo = false;
	private boolean isPrime = false;
	private String RawMessage;
	private boolean isAdmin = false;
	private boolean isStaff = false;
	
	
	//Constructor for ChatMessage class
	public ChatMessage(String botusername, String IRCmessage) {
		BotUsername = botusername;
		RawMessage = IRCmessage;
		
		//Get tags from the raw IRC message
		String[] tags = RawMessage.split(" ")[0].split(";");
		for (String tag : tags) {
			//Getting badges from message
			if (tag.contains("@badges=")) {
				String[] badgeList = tag.split("=");
				if (badgeList.length > 1){
					String badges = badgeList[1];
					for (String badge : badges.split(",")) {
						switch(badge.split("/")[0]) {
						case "premium":
							isPrime = true;
							break;
						case "subscriber":
							isSub = true;
							SubMonthCount = Integer.parseInt(badge.split("/")[1]);
							break;
						case "turbo":
							isTurbo = true;
							break;
						case "moderator":
						case "global_mod":
							isMod = true;
							break;
						case "staff":
							isStaff = true;
							break;
						}
					}
				}
			}
			//get username
			else if (tag.contains("display-name")) {
				Username = tag.split("=")[1].toLowerCase();
			}
			//get room id
			else if (tag.contains("room-id=")) {
				RoomId = tag.split("=")[1];
			}
			//get user id
			else if (tag.contains("user-id=")) {
				UserId = tag.split("=")[1];
			}
		}
		
		//Get channel name
		Channel = RawMessage.split(" ")[3].substring(1);
		
		//Get is sender if broadcaster
		isBroadcaster = (Channel.toLowerCase().equals(Username.toLowerCase()));
		
		//Parse Message
		Message = RawMessage.split(String.format("PRIVMSG #%s :", Channel))[1];
	}
	
	//Getter methods
	public String getBotUsername() {
		return BotUsername;
	}
	
	public String getUsername() {
		return Username;
	}
	
	public String getUserId() {
		return UserId;
	}
	
	public boolean getIsSub() {
		return isSub;
	}
	
	public boolean getIsAdmin() {
		return isAdmin;
	}
	
	public boolean getIsStaff() {
		return isStaff;
	}
	
	public int getSubMonthCount() {
		return SubMonthCount;
	}
	
	public boolean getIsPrime() {
		return isPrime;
	}
	
	public boolean getIsMod() {
		return isMod;
	}
	
	public boolean getIsTurbo() {
		return isTurbo;
	}
	
	public boolean getIsBroadcaster() {
		return isBroadcaster;
	}
	
	public String getRoomId() {
		return RoomId;
	}
	
	public String getRawMessage() {
		return RawMessage;
	}
	
	public String getMessage() {
		return Message;
	}
	
	public String getChannel() {
		return Channel;
	}
}
