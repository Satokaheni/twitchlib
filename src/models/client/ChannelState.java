package models.client;

public class ChannelState {

	private boolean r9k = false;
	private boolean sub_only = false;
	private boolean slow_mode = false;
	private boolean emote_only = false;
	private int followers_only = -1;
	private String broadcaster_language = null;
	private String channel = null;
	private String roomId = null;
	
	public ChannelState(String message) {
		String prop = message.split(" ")[0];
		for (String p: prop.split(";")) {
			String[] t = p.split("=");
			switch(p.split("=")[0].replaceAll("@", "")) {
			case "broadcaster-lang":
				if (t.length > 1)
					broadcaster_language = t[1];
				break;
			case "emote-only":
				if (t.length > 1)
					emote_only = Boolean.getBoolean(t[1]);
				break;
			case "r9k":
				if (t.length > 1)
					r9k = Boolean.getBoolean(t[1]);
				break;
			case "slow":
				if (t.length > 1)
					slow_mode = Boolean.getBoolean(t[1]);
				break;
			case "subs-only":
				if (t.length > 1)
					sub_only = Boolean.getBoolean(t[1]);
				break;
			case "followers-only":
				if (t.length > 1)
					followers_only = Integer.parseInt(t[1]);
				break;
			case "room-id":
				if (t.length > 1)
					roomId = t[1];
				break;
			}
		}
		
		channel = message.split("#")[1].toLowerCase();
	}
	
	//Accessor Methods
	public boolean getR9K() {
		return r9k;
	}
	
	public boolean getSubOnly() {
		return sub_only;
	}
	
	public boolean getEmoteOnly() {
		return emote_only;
	}
	
	public boolean getSlowMode() {
		return slow_mode;
	}
	
	public String getBroadcasterLanguage() {
		return broadcaster_language;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public int getFollowersOnly() {
		return followers_only;
	}
	
	public String getRoomId() {
		return roomId;
	}
}
