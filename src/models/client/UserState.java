package models.client;

import java.util.HashMap;

public class UserState {

	private HashMap<String, String> Badges = new HashMap<String, String>();;
	private String colorhex;
	private String display_name;
	private String emote_set;
	private String channel;
	private boolean subscriber;
	private boolean mod;
	private String userType;
	
	public UserState(String message) {
		for(String p: message.split(" ")[0].split(";")) {
			String[] t = p.split("=");
			switch(t[0]) {
			case "@badges":
				if (t.length > 1) {
					String badges = t[1];
					for(String b: badges.split(","))
						Badges.put(b.split("/")[0], b.split("/")[1]);
				}
				break;
			case "color":
				if (t.length > 1)
					colorhex = t[1];
				break;
			case "display-name":
				display_name = t[1];
				break;
			case "emote-sets":
				if (t.length > 1)
					emote_set = t[1];
				break;
			case "mod":
				mod = Boolean.getBoolean(t[1]);
				break;
			case "subscriber":
				subscriber = Boolean.getBoolean(t[1]);
				break;
			case "user-type":
				if (t.length > 1)
					userType = t[1];
				else
					userType = "viewer";
				break;
			}
		}
		
		channel = message.split(" ")[3].replaceAll("#", "");
		if (channel.toLowerCase() == display_name.toLowerCase())
			userType = "broadcaster";
	}
	
	//Accessor Methods
	public HashMap<String, String> getBadges() {
		return Badges;
	}
	
	public String getColor() {
		return colorhex;
	}
	
	public String getDisplayName() {
		return display_name;
	}
	
	public String getEmoteSet() {
		return emote_set;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public boolean isModerator() {
		return mod;
	}
	
	public boolean isSubscriber() {
		return subscriber;
	}
	
	public String getUserType() {
		return userType;
	}
}
