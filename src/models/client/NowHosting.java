package models.client;

public class NowHosting {
	private String channel;
	private String hosting_channel;
	
	public NowHosting(String irc) {
		channel = irc.split("#")[1].split(" ")[0];
		hosting_channel = irc.split(" ")[6].replaceAll("[.]", "");
	}
	
	//Accessor Methods
	public String getChannel() {
		return channel;
	}
	
	public String getHostingChannel() {
		return hosting_channel;
	}
}
