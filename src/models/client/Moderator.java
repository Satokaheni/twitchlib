package models.client;

public class Moderator {
	public String username;
	public String channel;
	
	public Moderator(String line) {
		username = line.split("o ")[1].toLowerCase();
		channel = line.split("MODE #")[1].split(" ")[0].toLowerCase();
	}
}
