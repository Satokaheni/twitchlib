package models.client;

public class UserBan {

	public String username;
	public String channel;
	public String reason;
	
	public UserBan(String message) {
		username = message.split(":")[1].toLowerCase();
		channel = message.split("CLEARCHAT #")[1].split(" ")[0].toLowerCase();
		reason = message.split(" ")[0].split("=")[1].toLowerCase();
	}
}
