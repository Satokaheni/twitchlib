package models.client;

public class Subscriber {
	private String UserId;
	private String Username;
	private String Message;
	private String Channel;
	private boolean isSub = false;
	private boolean isBroadcaster = false;
	private String RoomId;
	private boolean isMod = false;
	private boolean isTurbo = false;
	private boolean isPrime = false;
	private String RawMessage;
	private boolean isAdmin = false;
	private boolean isStaff = false;
	private String TmiSentTs;
	private int Months;
	private String SubPlanName;
	private String SystemMessage;
	
	public Subscriber(String IRCMessage) {
		RawMessage = IRCMessage;
		
		//Get tags from the raw IRC message
		String[] tags = RawMessage.split(" ")[0].split(";");
		for (String tag : tags) {
			//Getting badges from message
			if (tag.contains("@badges=")) {
				String[] badgeList = tag.split("=");
				if (badgeList.length > 1){
					String badges = badgeList[1];
					for (String badge : badges.split(",")) {
						switch(badge.split("/")[0]) {
						case "premium":
							isPrime = true;
							break;
						case "subscriber":
							isSub = true;
							break;
						case "turbo":
							isTurbo = true;
							break;
						case "moderator":
						case "global_mod":
							isMod = true;
							break;
						case "staff":
							isStaff = true;
							break;
						}
					}
				}
			}
			//get username
			else if (tag.contains("display-name")) {
				Username = tag.split("=")[1].toLowerCase();
			}
			//get room id
			else if (tag.contains("room-id=")) {
				RoomId = tag.split("=")[1];
			}
			//get user id
			else if (tag.contains("user-id=")) {
				UserId = tag.split("=")[1];
			}
			//get name of sub plan
			else if (tag.contains("msg-param-months")) {
				String plan = tag.split("=")[1];
				switch (plan) {
				case "prime":
					SubPlanName = "Prime";
					break;
				case "1000":
					SubPlanName = "Tier1";
					break;
				case "2000":
					SubPlanName = "Tier2";
					break;
				case "3000":
					SubPlanName = "Tier3";
					break;
				}
			}
			//Get System Message
			else if (tag.contains("system-msg")) {
				SystemMessage = tag.split("=")[1];
			}
			//Get tmi-sent
			else if (tag.contains("tmi-sent-ts")) {
				TmiSentTs = tag.split("=")[1];
			}
		}
		
		//Check if broadcaster sent it
		isBroadcaster = (Channel.toLowerCase() == Username.toLowerCase());
		
		//Get channel name
		Channel = RawMessage.split(" ")[2].substring(1);
		
		//Get Message
		Message = RawMessage.split(String.format("#%s :", Channel))[1];
	}
	
	//Getter methods
	
	public String getUsername() {
		return Username;
	}
	
	public String getUserId() {
		return UserId;
	}
	
	public boolean getIsSub() {
		return isSub;
	}
	
	public boolean getIsAdmin() {
		return isAdmin;
	}
	
	public boolean getIsStaff() {
		return isStaff;
	}
	
	public int getSubMonthCount() {
		return Months;
	}
	
	public boolean getIsPrime() {
		return isPrime;
	}
	
	public boolean getIsMod() {
		return isMod;
	}
	
	public boolean getIsTurbo() {
		return isTurbo;
	}
	
	public boolean getIsBroadcaster() {
		return isBroadcaster;
	}
	
	public String getRoomId() {
		return RoomId;
	}
	
	public String getRawMessage() {
		return RawMessage;
	}
	
	public String getMessage() {
		return Message;
	}
	
	public String getChannel() {
		return Channel;
	}

	public String getSubPlanName() {
		return SubPlanName;
	}
	
	public String getTmiSentTs() {
		return TmiSentTs;
	}
	
	public String getSystemMessage() {
		return SystemMessage;
	}
}
