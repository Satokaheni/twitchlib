package models.client;

import java.util.Arrays;
import java.util.ArrayList;

public class ChatCommand {
	private ChatMessage chatMessage;
	private String Command;
	private ArrayList<String> ArgsAsList = new ArrayList<String>();
	private String ArgsAsString = "";
	
	public ChatCommand(String ircMessage, ChatMessage chatMessage) {
		this.chatMessage = chatMessage;
		
		//get command
		Command = chatMessage.getMessage().split(" ")[0].substring(1);
		
		//get arguments to command if any
		if (chatMessage.getMessage().split(" ").length > 1) {
			
			//Get arguments as string
			StringBuilder builder = new StringBuilder();
			for (int i = 1; i < chatMessage.getMessage().split(" ").length - 1; i++) 
				builder.append(chatMessage.getMessage().split(" ")[i] + " ");
			builder.append(chatMessage.getMessage().split(" ")[chatMessage.getMessage().split(" ").length-1]);
			
			ArgsAsString = builder.toString();
			
			if (!chatMessage.getMessage().contains("\"")) {
				ArgsAsList = new ArrayList<String>(Arrays.asList(chatMessage.getMessage().split(" ")));
				ArgsAsList.remove(0);
			}
			else {
				//Handle quotes
				ArgsAsList = common.Helpers.parseQuotedMessage(ArgsAsString);
			}
		}
	}
	
	
}
