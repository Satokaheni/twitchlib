package models.client;

public class HostStop {

	private String hosting_channel;
	private int viewers = -1;
	
	public HostStop(String message) {
		hosting_channel = message.split(" ")[2].replaceAll("#", "").toLowerCase();
		if (message.split(" ").length > 4)
			viewers = Integer.parseInt(message.split(" ")[4]);
	}
	
	//Accessor Methods
	public String getHostingChannel() {
		return hosting_channel;
	}
	
	public int getViewers() {
		return viewers;
	}
}
