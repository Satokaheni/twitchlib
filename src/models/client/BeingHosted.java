package models.client;

public class BeingHosted {
	
	private String botUsername;
	private String hostedByChannel;
	private int viewers;
	private String channel;
	private boolean isAutoHosted;
	
	public BeingHosted(String botUsername, String hostedByChannel, String channel, int viewers, boolean auto) {
		this.botUsername = botUsername;
		this.hostedByChannel = hostedByChannel;
		this.channel = channel;
		this.viewers = viewers;
		this.isAutoHosted = auto;
	}
	
	//Accessor Methods
	public String getBotUsername() {
		return botUsername;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public String getHostedByChannel() {
		return hostedByChannel;
	}
	
	public int getViewers() {
		return viewers;
	}
	
	public boolean isAutoHost() {
		return isAutoHosted;
	}
}
