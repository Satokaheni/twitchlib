package connection;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

import models.client.*;
import services.MessageThrottler;
import events.client.*;
import events.services.*;

public class IRC implements Runnable{

	private static String HOST = "irc.chat.twitch.tv";
	private String NICK = "sato_chat";
	private static int PORT = 80;
	private String PASS = "oauth:u53r9or2zqejkdknxf5314cd2wpupz";
	private String channel = "";
	private boolean logging = false;
	private BufferedWriter irc_input;
	private BufferedReader irc_output;
	private Socket socket;
	private HashMap<String, JoinedChannel> joinedChannels = new HashMap<String, JoinedChannel>();
	private Thread server_output;
	private MessageThrottler chatThrottle = null;
	private boolean currentlyJoiningChannel = false;
	private Queue<String> joinQueue = new LinkedList<String>();
	
	//Events 
	private MessageListener messageListener = null;
	private CommandListener commandListener = null;
	private SubListener subListener = null;
	private ResubListener resubListener = null;
	private UserJoinedListener userJoinedListener = null;
	private UserLeftListener userLeftListener = null;
	private ClearChatListener chatClearListener = null;
	private UserBannedListener userBannedListener = null;
	private UserTimeoutListener userTimeoutListener = null;
	private ModeratorJoinListener moderatorJoinListener = null;
	private ModeratorPartListener moderatorPartListener = null;
	private ChannelStateChangedListener channelStateChangedListener = null;
	private UserStateChangedListener userStateChangedListener = null;
	private HostStartListener hostStartListener = null;
	private HostStopListener hostStopListener = null;
	private NowHostingListener nowHostingListener = null;
	private BeingHostedListener beingHostedListener = null;
	
	//Constructors 
	public IRC(String nick, String pass){
		this.NICK = nick;
		this.PASS = pass;
	}
	
	public IRC(String nick, String pass, boolean logging) {
		this(nick, pass);
		this.logging = logging;
	}
	
	public IRC(String nick, String pass, String channel) {
		this.NICK = nick;
		this.PASS = pass;
		this.channel = channel;
	}
	
	public IRC(String nick, String pass, String channel, boolean logging) {
		this(nick, pass, channel);
		this.logging = logging;
	}
	
	
	//Connect to the IRC server
	public void connect() throws UnknownHostException, IOException {
		//set up connection
		log("Connecting");
		socket = new Socket(HOST, PORT);
		irc_input = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		irc_output = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		log("Sending credentials");
		//Log on to the server
		irc_input.write(String.format("PASS %s\r\n", PASS));
		irc_input.write(String.format("NICK %s\r\n", NICK));
		//ask for membership
		log("Asking for membership");
		irc_input.write("CAP REQ :twitch.tv/membership\r\n");
		irc_input.write("CAP REQ :twitch.tv/tags\r\n");
		irc_input.write("CAP REQ :twitch.tv/commands\r\n");
		irc_input.flush();
		
		if (!channel.isEmpty()) {		
			//Join Channel
			log(String.format("Joining %s's channel", channel));
			irc_input.write(String.format("JOIN #%s\r\n", channel));
			irc_input.flush();
		}
		
		//Start receiving thread
		server_output = new Thread(this);
		server_output.start();
		
	}
	
	//Join a new channel
	public void joinChannel(String channel) throws IOException {
		//Check if already joined
		if (!joinedChannels.containsKey(channel.toLowerCase())) {
			joinQueue.add(channel.toLowerCase());
			if (!currentlyJoiningChannel)
				joinCheck();
		}
	}
	
	private void Join(String channel) throws IOException {
		channel = channel.toLowerCase();
		log(String.format("Joining %s's channel", channel));
		irc_input.write(String.format("JOIN #%s\r\n", channel));
		irc_input.flush();
		joinedChannels.put(channel, new JoinedChannel(channel));
	}
	
	//Leave a channel
	public void leaveChannel(String channel) throws IOException {
		//Check if in channel
		if (joinedChannels.containsKey(channel.toLowerCase()))
			Part(channel);
	}
	
	private void Part(String channel) throws IOException {
		channel = channel.toLowerCase();
		log(String.format("Leaving %s's channel", channel));
		irc_input.write(String.format("PART #%s\r\n", channel));
		irc_input.flush();
		joinedChannels.remove(channel);
	}
	
	//thread for receiving input from the server
	public void run() {
		String line = null;
		try {
			while ((line = irc_output.readLine()) != null) {
				log(line);
				parseIRCMessage(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Determine what the message is
	private void parseIRCMessage(String line) throws IOException {
		//On new subscriber
		if (common.Parsing.detectNewSub(line)){
			if (!(subListener == null)) 
				subListener.onNewSubscriber(this, new Subscriber(line));
			return;
		}
		
		//On resub
		if (common.Parsing.detectResub(line)) {
			if (!(resubListener == null)) 
				resubListener.onResubscriber(this, new Subscriber(line));
			return;
		}
		
		//On message received (Don't return to check for commands
		if (common.Parsing.detectMessage(line)) {
			if (!(messageListener == null))
				messageListener.onMessageReceived(this, new ChatMessage(NICK, line));
		}
		
		//On command Received
		if (common.Parsing.detectCommand(line)) {
			if (!(commandListener == null)) 
				commandListener.onCommandReceived(this, new ChatCommand(line, new ChatMessage(NICK, line)));
			return;
		}
		
		//On user join
		if (common.Parsing.detectUserJoin(line)) {
			String[] s = line.split("tmi.twitch.tv JOIN #");
			if (!(userJoinedListener == null)) 
				userJoinedListener.onUserJoined(this, new UserJoined(s[0].split("!")[0].toLowerCase(), s[1].toLowerCase()));
			return;
		}
		
		//On user part
		if (common.Parsing.detectUserPart(line)) {
			String[] s = line.split("tmi.twitch.tv PART #");
			if (!(userLeftListener == null)) 
				userLeftListener.onUserLeft(this, new UserLeft(s[0].split("!")[0].toLowerCase(), s[1].toLowerCase()));
			return;
		}
		
		//On CLEARCHAT
		if (common.Parsing.detectClearChat(line)) {
			if (!(chatClearListener == null)) 
				chatClearListener.onChatCleared(this, line.split("CLEARCHAT")[1].split(" ")[0]);
			return;
		}
		
		//On User Ban
		if (common.Parsing.detectUserBan(line)) {
			if (!(userBannedListener == null)) 
				userBannedListener.onUserBan(this, new UserBan(line));
			return;
		}
		
		//on User Timeout
		if (common.Parsing.detectUserTimeout(line)) {
			if (!(userTimeoutListener == null))
				userTimeoutListener.onUserTimeout(this, new UserTimeout(line));
			return;
		}
		
		//on Moderator Join
		if (common.Parsing.detectModeratorJoin(line)) {
			if (!(moderatorJoinListener == null)) 
				moderatorJoinListener.onModeratorJoin(this, new Moderator(line));
			return;
		}
		
		//on Moderator Part
		if (common.Parsing.detectModeratorLeave(line)) {
			if (!(moderatorPartListener == null)) 
				moderatorPartListener.onModeratorLeave(this, new Moderator(line));
			return;
		}
		
		//On Channel State Changed
		if (common.Parsing.detectRoomstate(line)) {
			if (!(channelStateChangedListener == null)) 
				channelStateChangedListener.onChannelStateChanged(this, new ChannelState(line));
			return;
		}
		
		//On userstate
		if (common.Parsing.detectUserState(line)) {
			if (!(userStateChangedListener == null)) 
				userStateChangedListener.onUserStateChange(this, new UserState(line));
			return;
		}
		
		//On Host Start
		if (common.Parsing.detectHostStart(line)) {
			if (!(hostStartListener == null)) 
				hostStartListener.onHostStarted(this, new HostStart(line));
			return;
		}
		
		//On Host Stop
		if (common.Parsing.detectHostStop(line)) {
			if (!(hostStopListener == null)) 
				hostStopListener.onHostingStopped(this, new HostStop(line));
			return;
		}
		
		//On now hosting someone
		if (common.Parsing.detectNowHosting(line, joinedChannels)) {
			if (!(nowHostingListener == null))
				nowHostingListener.onNowHosting(this, new NowHosting(line));			
			return;
		}
		
		//On Channel Join complate
		if (common.Parsing.detectJoinChannelCompleted(line)) {
			currentlyJoiningChannel = false;
			joinCheck();
			log("Finished Join");
			return;
		}
		
		//On being hosted
		if (common.Parsing.detectBeingHosted(line)) {
			if (!(beingHostedListener == null)) 
				beingHostedListener.onBeingHosted(this, new BeingHosted(NICK, line.split(":")[2].split(" ")[0], line.split(" ")[2], line.split(" ").length > 9 ? Integer.parseInt(line.split(" ")[8]) : -1, line.contains("now autohosting")));
			return;
		}
		
		//Check if PING
		if (common.Parsing.detectPING(line)) {
			this.SendRaw("PONG\r\n");
			return;
		}
	}

	//Add chatThottler to client
	public void addThrottle(MessageThrottler throttler) {
		this.chatThrottle = throttler;
	}
	
	//Send messages to sever
	public void SendRaw(String msg) {
		try {
			irc_input.write(msg);
			irc_input.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void SendMessage(String channel, String msg) {
		if (!(chatThrottle == null) && !chatThrottle.messagePermitted(msg)) 
			return;
		String message = String.format("%s!%s@%s.tmi.twitch.tv PRIVMSG #%s :%s", NICK, NICK, NICK, channel, msg);
		try {
			irc_input.write(String.format("%s\r\n", message));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void SendMessage(String msg) {
		if (joinedChannels.size() > 1) {
			SendMessage(joinedChannels.entrySet().iterator().next().getKey(), msg);
		}
		else
			log("You must join a channel first");
	}
	
	//Join Channels in Queue
	private void joinCheck() throws IOException {
		if(!joinQueue.isEmpty()) {
			currentlyJoiningChannel = true;
			String channel = joinQueue.poll();
			log(String.format("Joining Channel: %s", channel));
			Join(channel);
		}
	}
	
	private void log(String message) {
		if (this.logging) {
			System.out.println(message);
		}
	}
	
	//For when user add their own events 
	public void onMessageReceived(MessageListener listener) {
		messageListener = listener;
	}
	
	public void onCommandReceived(CommandListener listener) {
		commandListener = listener;
	}
	
	public void onSubscription(SubListener listener) {
		subListener = listener;
	}
	
	public void onResubscription(ResubListener listener) {
		resubListener = listener;
	}
		
	public void onUserJoined(UserJoinedListener listener) {
		userJoinedListener = listener;
	}
	
	public void onUserLeft(UserLeftListener listener) {
		userLeftListener = listener;
	}
	
	public void onChatClear(ClearChatListener listener) {
		chatClearListener = listener;
	}
	
	public void onUserBan(UserBannedListener listener) {
		userBannedListener = listener;
	}
	
	public void onUserTimeout(UserTimeoutListener listener) {
		userTimeoutListener = listener;
	}
	
	public void onModeratorJoin(ModeratorJoinListener listener) {
		moderatorJoinListener = listener;
	}
	
	public void onModeratorPart(ModeratorPartListener listener) {
		moderatorPartListener = listener;
	}
	
	public void onChannelStateChanged(ChannelStateChangedListener listener) {
		channelStateChangedListener = listener;
	}
	
	public void onUserStateChange(UserStateChangedListener listener) {
		userStateChangedListener = listener;
	}
	
	public void onHostStarted(HostStartListener listener) {
		hostStartListener = listener;
	}
	
	public void onHostStopped(HostStopListener listener) {
		hostStopListener = listener;
	}
	
	public void onNowHosting(NowHostingListener listener) {
		nowHostingListener = listener;
	}
	
	public void onBeingHosted(BeingHostedListener listener) {
		beingHostedListener = listener;
	}
	
	public void onMessageThrottled(MessageThrottledListener listener) {
		try {
			chatThrottle.onMessageThrottled(listener);
		}
		catch (Exception e) {
			System.out.println("Error Throttle Is Null must instantiate first");
		}
	}
	
	public void onThrottleReset(ThrottleResetListener listener) {
		try {
			chatThrottle.onThrottleReset(listener);
		}
		catch (Exception e) {
			System.out.println("Error Throttle Is Null must instantiate first");
		}
	}
}
