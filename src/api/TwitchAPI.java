package api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class TwitchAPI {

	private static String client_id;
	private static String auth_token;
	
	//Constructors
	public TwitchAPI(String client_id) {
		this.client_id = client_id;
	}
	
	public TwitchAPI(String client_id, String auth_token) {
		this(client_id);
		this.auth_token = auth_token;
	}
	
	
	////               HTTP CALL METHODS 
	
	//Request methods so don't have to retype get everywhere
	private static Future<HttpResponse<JsonNode>> get(String url) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> authGet(String url) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> authGetNoID(String url) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> get(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).queryString(query).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> authGet(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).queryString(query).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> authGetNoID(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.get(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).queryString(query).asJsonAsync();
	}
	
	//Put methods so don't have to retype everywhere
	private static Future<HttpResponse<JsonNode>> put(String url) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).asJsonAsync();		
	}
	
	private static Future<HttpResponse<JsonNode>> authPut(String url) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> authPutNoID(String url) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> put(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).queryString(query).asJsonAsync();		
	}
	
	private static Future<HttpResponse<JsonNode>> authPut(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).queryString(query).asJsonAsync();		
	}
	
	private static Future<HttpResponse<JsonNode>> authPutNoID(String url, Map<String, Object> query) throws UnirestException {
		return Unirest.put(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).queryString(query).asJsonAsync();		
	}
	
	//Delete methods so don't have to retype them
	private static Future<HttpResponse<JsonNode>> delete(String url) throws UnirestException {
		return Unirest.delete(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
	}
	
	private static Future<HttpResponse<JsonNode>> deleteNoId(String url) throws UnirestException {
		return Unirest.delete(url).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).asJsonAsync();
	}
	
	//Check for Error in API call
	private static boolean errorCheck(String call, HttpResponse<JsonNode> req) {
		if (req.getBody().getObject().has("error")) {
			System.out.println(String.format("Error in API call %s\nError Message: %s", call, req.getStatusText()));
			return true;
		}
		return false;
	}
	
	////					API CALL METHODS
	
	//Bits Endpoint
	public static class Bits {
		//get cheeremotes
		//multiple calls for optional parameters
		public static models.api.bits.CheerEmote getCheerEmotes() throws UnirestException, InterruptedException, ExecutionException { 
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/bits/actions");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCheerEmotes", response))
				return null;
			return new models.api.bits.CheerEmote(response.getBody().getObject().getJSONArray("actions"));
			
		}
		
		public static models.api.bits.CheerEmote getCheerEmotes(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException { 
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/bits/actions", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCheerEmotes", response))
				return null;
			return new models.api.bits.CheerEmote(response.getBody().getObject().getJSONArray("actions"));
			
		}
	}
	
	//Channel Feed Endpoint
	public static class ChannelFeed {
		//Get Multiple Feed Post auth optional
		//Multiple methods for optional parameters
		public static models.api.channelfeed.MultipleFeedPosts getMultipleFeedPost(String channel_id, boolean auth) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id));
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getMultipleFeedPost", response))
				return null;
			return new models.api.channelfeed.MultipleFeedPosts(response.getBody().getObject());
		}
		
		public static models.api.channelfeed.MultipleFeedPosts getMultipleFeedPost(String channel_id, boolean auth, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id), parameters);
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getMultipleFeedPost", response))
				return null;
			return new models.api.channelfeed.MultipleFeedPosts(response.getBody().getObject());
		}
		
		//Get Feed Post String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id)
		//Multiple Methods for optional parameters
		public static models.api.channelfeed.FeedPost getFeedPost(String channel_id, String post_id, boolean auth) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id));
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPost(response.getBody().getObject());
		}
		
		public static models.api.channelfeed.FeedPost getFeedPost(String channel_id, String post_id, boolean auth, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id), parameters);
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPost(response.getBody().getObject());
		}
		
		//Create FeedPost #requires auth
		//Multiple Methods for Optional Paraeters
		public static models.api.channelfeed.FeedPostCreation createFeedPost(String channel_id, String content) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("content", content);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPostCreation(response.getBody().getObject());
		}
		
		public static models.api.channelfeed.FeedPostCreation createFeedPost(String channel_id, String content, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("content", content);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/feed/%s/posts", channel_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").queryString(parameters).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPostCreation(response.getBody().getObject());
		}
		
		//Delete FeedPost #requires Auth
		public static models.api.channelfeed.FeedPost deleteFeedPost(String channel_id, String post_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s", channel_id, post_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPost(response.getBody().getObject());
		}
		
		//Create Reaction to Feed Post #requires Auth
		public static models.api.channelfeed.FeedPostReactionPost createReactionToFeedPost(String channel_id, String post_id, String emote_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/reactions?emote_id=%s", channel_id, post_id, emote_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createReactionToFeedPost", response))
				return null;
			return new models.api.channelfeed.FeedPostReactionPost(response.getBody().getObject());
		}
		
		//Delete Reaction to Feed Post #requires auth
		public static boolean deleteReactionToFeedPost(String channel_id, String post_id, String emote_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/reactions?emote_id=%s", channel_id, post_id, emote_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteReactionToFeedPost", response))
				return false;
			return response.getBody().getObject().getBoolean("deleted");
		}
		
		//Get Feed Comments
		//Multiple Methods for Optional Parameters
		public static models.api.channelfeed.FeedPostComments getFeedComments(String channel_id, String post_id, boolean auth) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments", channel_id, post_id));
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments", channel_id, post_id));	
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeedComments", response))
				return null;
			return new models.api.channelfeed.FeedPostComments(response.getBody().getObject());
		}
		
		public static models.api.channelfeed.FeedPostComments getFeedComments(String channel_id, String post_id, boolean auth, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future;
			if (auth)
				future = authGet(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments", channel_id, post_id), parameters);
			else
				future = get(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments", channel_id, post_id), parameters);	
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeedComments", response))
				return null;
			return new models.api.channelfeed.FeedPostComments(response.getBody().getObject());
		}
		
		//Create Feed Comment #requires auth
		public static models.api.channelfeed.FeedPostComment createFeedComment(String channel_id, String post_id, String content) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments?content=%s", channel_id, post_id, content)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createFeedPostComment", response))
				return null;
			return new models.api.channelfeed.FeedPostComment(response.getBody().getObject());
		}
		
		//Delete Feed Comment #requires auth
		public static models.api.channelfeed.FeedPostComment deleteFeedComment(String channel_id, String post_id, String comment_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments/%s", channel_id, post_id, comment_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteFeedPostComment", response))
				return null;
			return new models.api.channelfeed.FeedPostComment(response.getBody().getObject());
		}
		
		//Create Reaction to Feed Post Comment #requires auth
		public static models.api.channelfeed.FeedPostReactionPost createReactionToFeedPostComment(String channel_id, String post_id, String comment_id, String emote_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments/%s/reactions?emote_id=%s", channel_id, post_id, comment_id, emote_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createReactionToFeedPostComment", response))
				return null;
			return new models.api.channelfeed.FeedPostReactionPost(response.getBody().getObject());
		}
		
		//Delete Reaction to Feed Post Comment #requires auth
		public static boolean deleteReactionToFeedPostComment(String channel_id, String post_id, String comment_id, String emote_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/feed/%s/posts/%s/comments/%s/reactions?emote_id=%s", channel_id, post_id, comment_id, emote_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteReactionToFeedPostComment", response)) 
				return false;
			return response.getBody().getObject().getBoolean("deleted");
		}
	}
	
	//Channels Endpoint
	public static class Channels {
		//Get Channel #requires auth
		public static models.api.channels.ChannelAuthed getChannel() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/channel");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannel", response))
				return null;
			return new models.api.channels.ChannelAuthed(response.getBody().getObject());
		}
		
		//Get Channel by Id
		public static models.api.channels.Channel getChannelById(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelById", response))
				return null;
			return new models.api.channels.Channel(response.getBody().getObject());
		}
		
		//Update Channel #auth required Map required
		public static models.api.channels.Channel updateChannel(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject subbody = new JSONObject();
			for (String key : parameters.keySet()) {
				if (key == "channel_feed_enabled")
					subbody.put(key, (Boolean) parameters.get(key));
				else
					subbody.put(key, parameters.get(key).toString());
			}
			JSONObject body = new JSONObject();
			body.put("channel", subbody);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/channels/%s", id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateChannel", response))
				return null;
			return new models.api.channels.Channel(response.getBody().getObject());
		}
		
		//Get channel editors #requires auth
		public static models.api.channels.ChannelEditors getChannelEditors(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/channels/%s/editors", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelEditors", response))
				return null;
			return new models.api.channels.ChannelEditors(response.getBody().getObject().getJSONArray("users"));
		}
		
		//Get Channel Followers
		//Multiple Methods for optional parameters
		public static models.api.channels.ChannelFollowers getChannelFollowers(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/follows", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelFollowers", response))
				return null;
			return new models.api.channels.ChannelFollowers(response.getBody().getObject());
		}
		
		public static models.api.channels.ChannelFollowers getChannelFollowers(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/follows", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelFollowers", response))
				return null;
			return new models.api.channels.ChannelFollowers(response.getBody().getObject());
		}
		
		//Get Channel Teams
		public static models.api.channels.ChannelTeams getChannelTeams(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/teams", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelTeams", response))
				return null;
			return new models.api.channels.ChannelTeams(response.getBody().getObject().getJSONArray("teams"));
		}
		
		//Get Channel Subscribers #requires Auth
		//Multiple Methods for optional parameters
		public static models.api.channels.ChannelSubscribers getChannelSubscribers(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/channels/%s/subscriptions", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelSubscribers", response))
				return null;
			return new models.api.channels.ChannelSubscribers(response.getBody().getObject());
		}
		
		public static models.api.channels.ChannelSubscribers getChannelSubscribers(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/channels/%s/subscriptions", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelSubscribers", response))
				return null;
			return new models.api.channels.ChannelSubscribers(response.getBody().getObject());
		}
		
		//Check Channel Subscription By User #requires auth
		public static models.api.users.Subscription getChannelSubscriptionByUser(String channel_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/channels/%s/subscriptions/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelSubsciptionByUser", response))
				return null;
			return new models.api.users.Subscription(response.getBody().getObject());
		}
		
		//Get channel videos 
		//Multiple Methods for optional parameters
		public static models.api.channels.ChannelVideos getChannelVideos(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/videos", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelVideos", response))
				return null;
			return new models.api.channels.ChannelVideos(response.getBody().getObject());
		}
		
		public static models.api.channels.ChannelVideos getChannelVideos(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/videos", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelVideos", response))
				return null;
			return new models.api.channels.ChannelVideos(response.getBody().getObject());
		}
		
		//Start Channel Commercial #requires auth
		public static models.api.channels.ChannelCommercial startChannelCommercial(String id, int duration) throws UnirestException, InterruptedException, ExecutionException {
			
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/channels/%s/commercial", id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").field("length", duration).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("startChannelCommercial", response))
				return null;
			return new models.api.channels.ChannelCommercial(response.getBody().getObject());
		}
		
		//Reset Channel Stream key #requires auth
		public static models.api.channels.ChannelAuthed resetChannelStreamKey(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/channels/%s/stream_key", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("resetChannelStreamKey", response))
				return null;
			return new models.api.channels.ChannelAuthed(response.getBody().getObject());
		}
		
		//Get Channel Community #requires auth
		public static models.api.communities.Community getChannelCommunity(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/community", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChannelCommunity", response))
				return null;
			return new models.api.communities.Community(response.getBody().getObject());
		}
		
		//Set Channel Community #requires auth
		public static boolean setChannelCommunity(String channel_id, String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = put(String.format("https://api.twitch.tv/kraken/channels/%s/community/%s", channel_id, community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("setChannelCommunity", response))
				return false;
			return true;
		}
		
		//Delete Community from Channel
		public static boolean deleteChannelCommunity(String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/channels/%s/community", channel_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deletChannelCommunity", response)) 
				return false;
			return true;
		}
	}
	
	//Chat Endpoint
	public static class Chat {
		//Get Chat Badges
		public static models.api.chat.ChatBadges getBadges(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/chat/%s/badges", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getBadges", response))
				return null;
			return new models.api.chat.ChatBadges(response.getBody().getObject());
		}
		
		//Get Chat Emoticons By Set
		//Multiple Methods for optional paramters
		public static models.api.chat.EmoteSet getChatEmoticonsBySet() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/chat/emoticon_images");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChatEmoticonsBySet", response))
				return null;
			return new models.api.chat.EmoteSet(response.getBody().getObject());
		}
		
		public static models.api.chat.EmoteSet getChatEmoticonsBySet(List<Integer> emotesets) throws UnirestException, InterruptedException, ExecutionException {
			Map<String, Object> query = new HashMap<String, Object>();
			query.put("emotesets", emotesets);
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/chat/emoticon_images", query);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getChatEmoticonsBySet", response))
				return null;
			return new models.api.chat.EmoteSet(response.getBody().getObject());
		}
		
		//Get All Chat Emoticons
		public static models.api.chat.AllChatEmotes getAllChatEmoticons() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/chat/emoticons");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getAllChatEmoticons", response))
				return null;
			return new models.api.chat.AllChatEmotes(response.getBody().getObject().getJSONArray("emoticons"));
		}
	}
	
	//Clips Endpoint
	public static class Clips {
		//Get Clip By Slug ID
		public static models.api.clips.Clip getClip(String slug) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/clips/%s", slug));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getClip", response))
				return null;
			return new models.api.clips.Clip(response.getBody().getObject());
		}
		
		//Get Top Clips
		//Multiple Methods for optional parameters
		public static models.api.clips.Clips getTopClips() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/clips/top");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopClips", response))
				return null;
			return new models.api.clips.Clips(response.getBody().getObject().getJSONArray("clips"));
		}
		
		public static models.api.clips.Clips getTopClips(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/clips/top", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopClips", response))
				return null;
			return new models.api.clips.Clips(response.getBody().getObject().getJSONArray("clips"));
		}
		
		//Get clips followed by specified user #requires auth
		//Multiple methods for optional parameters
		public static models.api.clips.Clips getFollowedClips() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/clips/followed");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedClips", response))
				return null;
			return new models.api.clips.Clips(response.getBody().getObject().getJSONArray("clips"));
		}
		
		public static models.api.clips.Clips getFollowedClips(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/clips/followed", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedClips", response))
				return null;
			return new models.api.clips.Clips(response.getBody().getObject().getJSONArray("clips"));
		}
	}
	
	//Collections Endpoint
	public static class Collections {
		//Get Collection Metadata
		public static models.api.collections.CollectionMetaData getCollectionMetaData(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/collections/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCollectionMetaData", response))
				return null;
			return new models.api.collections.CollectionMetaData(response.getBody().getObject());
		}
		
		//Get Collection
		//Multiple Methods for optional parameters
		public static models.api.collections.Collection getCollection(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/collections/%s/items", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCollection", response))
				return null;
			return new models.api.collections.Collection(response.getBody().getObject());
		}
		
		public static models.api.collections.Collection getCollection(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/collections/%s/items", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCollection", response))
				return null;
			return new models.api.collections.Collection(response.getBody().getObject());
		}
		
		//Get Collections by Channel
		//Multiple Methods for optional parameters
		public static models.api.collections.CollectionsByChannel getCollectionsByChannel(String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/collections", channel_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCollectionsByChannel", response)) 
				return null;
			return new models.api.collections.CollectionsByChannel(response.getBody().getObject());
		}
		
		public static models.api.collections.CollectionsByChannel getCollectionsByChannel(String channel_id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/channels/%s/collections", channel_id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCollectionsByChannel", response)) 
				return null;
			return new models.api.collections.CollectionsByChannel(response.getBody().getObject());
		}
		
		//Create Collection #requires auth
		public static models.api.collections.CollectionMetaData createCollection(String channel_id, String title) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("title", title);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/channels/%s/collections", channel_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createCollection", response))
				return null;
			return new models.api.collections.CollectionMetaData(response.getBody().getObject());
		}
		
		//Update Collection #requires Auth
		public static boolean updateCollection(String collection_id, String title) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("title", title);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/collections/%s", collection_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateCollection", response))
				return false;
			return true;
		}
		
		//Create Collection Thumbnail 
		public static boolean createCollectionThumbnail(String collection_id, String item_id) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("item_id", item_id);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/collections/%s/thumbnail", collection_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if(errorCheck("createCollectionThumbnail", response))
					return false;
			return true;
		}
		
		//Delete Collection #requires auth
		public static boolean deleteCollection(String collection_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/collections/%s", collection_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteCollection", response))
				return false;
			return true;
		}
		
		//Add Item to Collection #requires auth
		public static models.api.collections.CollectionItem addItemToCollection(String collection_id, String id) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("id", id);
			body.put("type", "video");
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/collections/%s/items", collection_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("addItemToCollection", response))
				return null;
			
			return new models.api.collections.CollectionItem(response.getBody().getObject());
		}
		
		//Delete Item from Collection #requires auth
		public static boolean deleteItemFromCollection(String collection_id, String item_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/collections/%s/items/%s", collection_id, item_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteItemFromCollection", response)) 
				return false;
			return true;
		}
		
		//Move Item Within Collection #requires auth
		public static boolean moveItemWithinCollection(String collection_id, String item_id, String position) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("position", position);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https;//api.twitch.tv/kraken/collections/%s/items/%s", collection_id, item_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("moveItemWithinCollection", response))
				return false;
			return true;
		}
	}
	
	//Communities Endpoint
	public static class Communities {
		//Get community by name
		public static models.api.communities.Community getCommunityByName(String name) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/communities?name=%s", name));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityByName", response))
				return null;
			return new models.api.communities.Community(response.getBody().getObject());
		}
		
		//Get Community by ID
		public static models.api.communities.Community getCommunityById(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/communities/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityById", response))
				return null;
			return new models.api.communities.Community(response.getBody().getObject());
		}
		
		//Update Community #requries auth
		//Multiple Methods for optional parameters
		public static boolean updateCommunity(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/communities/%s", id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Content-Type", "application/json").header("Authorization", auth_token).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateCommunity", response))
				return false;
			return true;
		}
		
		public static boolean updateCommunity(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			for (String key: parameters.keySet()) 
				body.put(key, parameters.get(key).toString());
			
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/communities/%s", id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Content-Type", "application/json").header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateCommunity", response))
				return false;
			return true;
		}
		
		//Get Top Communities
		//Multiple Methods for optional paramters
		public static models.api.communities.TopCommunities getTopCommunities() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/communities/top");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopCommunities", response))
				return null;
			return new models.api.communities.TopCommunities(response.getBody().getObject());
		}
		
		public static models.api.communities.TopCommunities getTopCommunities(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/communities/top", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopCommunities", response))
				return null;
			return new models.api.communities.TopCommunities(response.getBody().getObject());
		}
		
		//Get Community Banned Users #requires auth
		//Multiple methods for optional parameters
		public static models.api.communities.BannedUsers getCommunityBannedUsers(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGetNoID(String.format("https://api.twitch.tv/kraken/communities/%s/bans", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityBannedUsers", response))
				return null;
			return new models.api.communities.BannedUsers(response.getBody().getObject());
		}
		
		public static models.api.communities.BannedUsers getCommunityBannedUsers(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGetNoID(String.format("https://api.twitch.tv/kraken/communities/%s/bans", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityBannedUsers", response))
				return null;
			return new models.api.communities.BannedUsers(response.getBody().getObject());
		}
		
		//Ban Community User #requires auth
		public static boolean banCommunityUser(String community_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPutNoID(String.format("https://api.twitch.tv/kraken/communities/%s/bans/%s", community_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("banCommunityUser", response))
				return false;
			return true;
		}
		
		//UnBan Community User #requires auth
		public static boolean unBanCommunityUser(String community_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/communities/%s/bans/%s", community_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("unBanCommunityUser", response))
				return false;
			return true;
		}
		
		//Create Community Avatar Image #requires auth
		public static boolean createCommunityAvatarImage(String community_id, String avatar_image) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("avatar_image", avatar_image);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/communities/%s/images/avatar", community_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Content-Type", "application/json").header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createCommunityAvatarImage", response))
				return false;
			return true;
		}
		
		//Delete Community Avatar Image #requires auth
		public static boolean deleteCommunityAvatarImage(String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/communities/%s/images/avatar", community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteCommunityAvatarImage", response))
				return false;
			return true;
		}
		
		//Create Community Cover Image #requires auth
		public static boolean createCommunityCoverImage(String community_id, String cover_image) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("cover_image", cover_image);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/communities/%s/images/cover", community_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Content-Type", "application/json").header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createCommunityAvatarImage", response))
				return false;
			return true;
		}
		
		//Delete Community CoverImage #requires auth
		public static boolean deleteCommunityCoverImage(String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/communities/%s/images/cover", community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteCommunityAvatarImage", response))
				return false;
			return true;
		}
		
		//Get Community Moderators #requires auth
		public static models.api.communities.Moderators getCommunityModerators(String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/communities/%s/moderators", community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityModerators", response))
				return null;
			return new models.api.communities.Moderators(response.getBody().getObject());
		}
		
		//Add Community Moderator #requires auth
		public static boolean addCommunityModerator(String community_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPutNoID(String.format("https://api.twitch.tv/kraken/communities/%s/moderators/%s", community_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("addCommunityModerator", response))
				return false;
			return true;
		}
		
		//Delete Community Moderator #requires auth
		public static boolean deleteCommunityModerator(String community_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/communities/%s/moderators/%s", community_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteCommunityModerator", response))
				return false;
			return true;
		}
		
		//Get community permissions #requires auth
		public static models.api.communities.Permissions getCommunityPermissions(String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGetNoID(String.format("https://api.twitch.tv/kraken/communities/%s/permissions", community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityPermissions", response))
				return null;
			return new models.api.communities.Permissions(response.getBody().getObject());
		}
		
		//Report Community Violation
		public static boolean reportCommunityViolation(String community_id, String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("channel_id", channel_id);
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/communities/%s/report_channel", community_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).header("Content-Type", "application/json").body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("reportCommunityViolation", response))
				return false;
			return true;
		}
		
		//Get Community Timed Out Users #requires auth
		//Multiple Methods for optional parameters
		public static models.api.communities.TimedOutUsers getCommunityTimedOutUsers(String community_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGetNoID(String.format("https://api.twitch.tv/kraken/communities/%s/timeouts", community_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityTimedOutusers", response))
				return null;
			return new models.api.communities.TimedOutUsers(response.getBody().getObject());
		}
		
		public static models.api.communities.TimedOutUsers getCommunityTimedOutUsers(String community_id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGetNoID(String.format("https://api.twitch.tv/kraken/communities/%s/timeouts", community_id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getCommunityTimedOutusers", response))
				return null;
			return new models.api.communities.TimedOutUsers(response.getBody().getObject());
		}
		
		//Add Community Timed Out User #requires Auth
		//Multiple Methods for optional parameters
		public static boolean addCommunityTimedOutUser(String community_id, String user_id, int duration) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("duration", duration);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/communities/%s/timeouts/%s", community_id, user_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("addCommunityTimedOutUser", response))
				return false;
			return true;
		}
		
		public static boolean addCommunityTimedOutUser(String community_id, String user_id, int duration, String reason) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("duration", duration);
			body.put("reason", reason);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/communities/%s/timeouts/%s", community_id, user_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("addCommunityTimedOutUser", response))
				return false;
			return true;
		}
		
		//Delete Community Timed Out User
		public static boolean deleteCommunityTimedOutUser(String community_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = deleteNoId(String.format("https://api.twitch.tv/kraken/communities/%s/timeouts/%s", community_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteCommunityTimedOutUser", response))
				return false;
			return true;
		}
	}
	
	//Games Endpoint
	public static class Games {
		//Get Top Games
		//Multiple methods for optional parameters
		public static models.api.games.Games getTopGames() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/games/top");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopGames", response))
				return null;
			return new models.api.games.Games(response.getBody().getObject());
		}
		
		public static models.api.games.Games getTopGames(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/games/top", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopGames", response))
				return null;
			return new models.api.games.Games(response.getBody().getObject());
		}
	}
	
	//Ingest Enpoit
	public static class Ingests {
		//Get Ingest Server List
		public static models.api.ingests.Ingests getIngestSeverList() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/ingests");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getIngestServerList", response))
				return null;
			return new models.api.ingests.Ingests(response.getBody().getObject().getJSONArray("ingests"));
		}
	}
	
	//Search Endpoint
	public static class Search {
		//Search Channels
		//Multiple Methods for optional parameters
		public static models.api.search.SearchChannels searchChannels(String query) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/channels?query=%s", query));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchChannels", response))
				return null;
			return new models.api.search.SearchChannels(response.getBody().getObject());
		}
		
		public static models.api.search.SearchChannels searchChannels(String query, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/channels?query=%s", query), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchChannels", response))
				return null;
			return new models.api.search.SearchChannels(response.getBody().getObject());
		}
		
		//Search Games
		//Multiple Methods for optional parameters
		public static models.api.search.SearchGames searchGames(String query) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/games?query=", query));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchGames", response))
				return null;
			return new models.api.search.SearchGames(response.getBody().getObject());
		}
		
		public static models.api.search.SearchGames searchGames(String query, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/games?query=", query), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchGames", response))
				return null;
			return new models.api.search.SearchGames(response.getBody().getObject());
		}
		
		//Search Streams
		//Multiple Methods for optional parameters
		public static models.api.search.SearchStreams searchStreams(String query) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/streams?=query=%s", query));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchStreams", response))
				return null;
			return new models.api.search.SearchStreams(response.getBody().getObject());
		}
		
		public static models.api.search.SearchStreams searchStreams(String query, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/search/streams?=query=%s", query), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("searchStreams", response))
				return null;
			return new models.api.search.SearchStreams(response.getBody().getObject());
		}
	}
	
	//Streams Endpoint
	public static class Streams {
		//Get stream by user id
		//Multiple Methods for optional parameters
		public static models.api.streams.Stream getStreamByUser(String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/streams/%s", user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getStreamByUser", response) || response.getBody().getObject().getJSONObject("stream").equals(null))
				return null;
			return new models.api.streams.Stream(response.getBody().getObject().getJSONObject("stream"));
		}
		
		public static models.api.streams.Stream getStreamByUser(String user_id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/streams/%s", user_id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getStreamByUser", response) || response.getBody().getObject().getJSONObject("stream").equals(null))
				return null;
			return new models.api.streams.Stream(response.getBody().getObject().getJSONObject("stream"));
		}
		
		//Get Live Streams
		//Multiple Methods for optional parameters
		public static models.api.streams.LiveStreams getLiveStreams() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getLiveStreams", response))
				return null;
			return new models.api.streams.LiveStreams(response.getBody().getObject());
		}
		
		public static models.api.streams.LiveStreams getLiveStreams(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getLiveStreams", response))
				return null;
			return new models.api.streams.LiveStreams(response.getBody().getObject());
		}
		
		//Get Streams Summary
		//Multiple Methods for optional parameters
		public static models.api.streams.StreamsSummary getStreamsSummary() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams/summary");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getStreamsSummary", response))
				return null;
			return new models.api.streams.StreamsSummary(response.getBody().getObject());
		}
		
		public static models.api.streams.StreamsSummary getStreamsSummary(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams/summary", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getStreamsSummary", response))
				return null;
			return new models.api.streams.StreamsSummary(response.getBody().getObject());
		}
		
		//Get Featured Streams
		//Multiple Methods for optional parameters
		public static models.api.streams.FeaturedStreams getFeaturedStreams() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams/featured");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeaturedStreams", response))
				return null;
			return new models.api.streams.FeaturedStreams(response.getBody().getObject());
		}
		
		public static models.api.streams.FeaturedStreams getFeaturedStreams(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/streams/featured", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFeaturedStreams", response))
				return null;
			return new models.api.streams.FeaturedStreams(response.getBody().getObject());
		}
		
		//Get Followed Streams #requires auth
		//Multiple methods for optional parameters
		public static models.api.streams.FollowedStreams getFollowedStreams() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/streams/followed");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedStreams", response))
				return null;
			return new models.api.streams.FollowedStreams(response.getBody().getObject());
		}
		
		public static models.api.streams.FollowedStreams getFollowedStreams(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/streams/followed", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedStreams", response))
				return null;
			return new models.api.streams.FollowedStreams(response.getBody().getObject());
		}
		
		//Get uptime of a stream
		public String getUptime(String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			DateTime current = common.Parsing.convertToDateTime(new org.joda.time.DateTime(org.joda.time.DateTimeZone.UTC).toString());
			int seconds = Seconds.secondsBetween(Streams.getStreamByUser(channel_id).getCreatedAt(), current).getSeconds();
			return common.Parsing.getTimeOutput(seconds);
		}
		
		//Get if broadcaster is online
		public boolean broadcasterOnline(String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			return (Streams.getStreamByUser(channel_id).equals(null));
		}
	}
	
	//Teams Endpoint
	public static class Teams {
		//Get all Teams
		//Multiple Methods for optional parameters
		public static models.api.teams.Teams getAllTeams() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/teams");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getAllTeams", response))
				return null;
			return new models.api.teams.Teams(response.getBody().getObject().getJSONArray("teams"));
		}
		
		public static models.api.teams.Teams getAllTeams(Map<String, Object> query) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/teams", query);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getAllTeams", response))
				return null;
			return new models.api.teams.Teams(response.getBody().getObject().getJSONArray("teams"));
		}
		
		//Get Team
		public static models.api.teams.Team getTeam(String team) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/teams/%s", team));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTeam", response))
				return null;
			return new models.api.teams.Team(response.getBody().getObject());
		}
	}
	
	//Users Endpoint
	public static class Users {
		//Get user by name
		public static models.api.users.Users getUserByName(String name) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/users?login=%s", name));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserByName", response)) 
				return null;
			return new models.api.users.Users(response.getBody().getObject().getJSONArray("users"));
		}
		
		//Get user by id
		public static models.api.users.User getUserById(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/users/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserById", response))
				return null;
			return new models.api.users.User(response.getBody().getObject());
		}
		
		//Get users emote set #requires auth
		public static models.api.users.EmoteSet getUserEmotes(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/users/%s/emotes", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserEmotes", response))
				return null;
			return new models.api.users.EmoteSet((JSONObject) response.getBody().getObject().get("emoticon_sets"));
		}
		
		//Get if user is subscribed to channel #requires auth
		public static models.api.users.Subscription UserSubscribedChannel(String channel_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet(String.format("https://api.twitch.tv/kraken/users/%s/subscriptions/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("UserSubscribedChannel", response)) {
				return null;
			}
			return new models.api.users.Subscription(response.getBody().getObject());
		}
		
		//Get follows of a channel
		//Multiple methods for each optional parameter
		public static models.api.users.UserFollows getUserFollows(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserFollows", response)) 
				return null;
			return new models.api.users.UserFollows(response.getBody().getObject().getJSONArray("follows"));
		}
		
		public static models.api.users.UserFollows getUserFollows(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserFollows", response)) 
				return null;
			return new models.api.users.UserFollows(response.getBody().getObject().getJSONArray("follows"));
		}
		
		//Check if a user follows a channel
		public static models.api.users.UserFollow UserFollowsChannel(String channel_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("UserFollowsChannel", response))
				return null;
			return new models.api.users.UserFollow(response.getBody().getObject());
		}
		
		//Follow a channel #requires auth
		//Multiple methods because of parameters
		public static models.api.users.UserFollow FollowChannel(String user_id, String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = put(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("FollowChannel", response)) 
				return null;
			return new models.api.users.UserFollow(response.getBody().getObject());
		}
		
		public static models.api.users.UserFollow FollowChannel(String user_id, String channel_id, boolean notifications) throws UnirestException, InterruptedException, ExecutionException {
			JSONObject body = new JSONObject();
			body.put("notifications", notifications);
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels/%s", channel_id, user_id)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).body(body).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("FollowChannel", response)) 
				return null;
			return new models.api.users.UserFollow(response.getBody().getObject());
		}
		
		//Unfollow Channel #requires auth
		public static boolean UnFollowChannel(String user_id, String channel_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/users/%s/follows/channels/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("UnFollowChannel", response)) 
				return false;
			return true;
		}
		
		// Get Users Block List #requires auth
		// Multiple Methods for different parameters
		public static models.api.users.UserBlocks getUserBlocks(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPut(String.format("https://api.twitch.tv/kraken/users/%s/blocks", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserBlocks", response)) 
				return null;
			return new models.api.users.UserBlocks(response.getBody().getObject().getJSONArray("blocks"));
		}
		
		public static models.api.users.UserBlocks getUserBlocks(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPut(String.format("https://api.twitch.tv/kraken/users/%s/blocks", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getUserBlocks", response)) 
				return null;
			return new models.api.users.UserBlocks(response.getBody().getObject().getJSONArray("blocks"));
		}
		
		//Block User from Channel #requires auth
		public static models.api.users.UserBlock BlockUser(String channel_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPut(String.format("https://api.twitch.tv/users/%s/blocks/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("BlockUser", response))
				return null;
			return new models.api.users.UserBlock(response.getBody().getObject());
		}
		
		//Unblock User from Channel
		public static boolean UnblockUser(String channel_id, String user_id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/users/%s/blocks/%s", channel_id, user_id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("UnblockUser", response))
				return false;
			return true;
		}
	}
	
	//Video endpoint
	public static class Videos {
		//Get Video
		public static models.api.videos.Video getVideo(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get(String.format("https://api.twitch.tv/kraken/videos/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getVideo", response))
				return null;
			return new models.api.videos.Video(response.getBody().getObject());
		}
		
		//Get Top Videos
		//Multiple Methods for optional parameters
		public static models.api.videos.TopVideos getTopVideos() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/videos/top");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopVideos", response))
				return null;
			return new models.api.videos.TopVideos(response.getBody().getObject());
		}
		
		public static models.api.videos.TopVideos getTopVideos(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = get("https://api.twitch.tv/kraken/videos/top", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getTopVideos", response))
				return null;
			return new models.api.videos.TopVideos(response.getBody().getObject());
		}
		
		//Get Followed Videos #requires auth
		//Multiple Methods for optional parameters
		public static models.api.videos.FollowedVideos getFollowedVideo() throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/videos/followed");
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedVideos", response))
				return null;
			return new models.api.videos.FollowedVideos(response.getBody().getObject());
		}
		
		public static models.api.videos.FollowedVideos getFollowedVideo(Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authGet("https://api.twitch.tv/kraken/videos/followed", parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("getFollowedVideos", response))
				return null;
			return new models.api.videos.FollowedVideos(response.getBody().getObject());
		}
		
		//Create Video #requires auth
		//Multiple Methods for optional parameters
		public static models.api.videos.VideoUpload createVideo(String channel_id, String video_title) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/videos?channel_id=%s&title=%s", channel_id, video_title)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createVideo", response))
				return null;
			return new models.api.videos.VideoUpload(response.getBody().getObject());
		}
		
		public static models.api.videos.VideoUpload createVideo(String channel_id, String video_title, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://api.twitch.tv/kraken/videos?channel_id=%s&title=%s", channel_id, video_title)).header("Accept", "application/vnd.twitchtv.v5+json").header("Client-ID", client_id).header("Authorization", auth_token).queryString(parameters).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("createVideo", response))
				return null;
			return new models.api.videos.VideoUpload(response.getBody().getObject());
		}
		
		//Upload Video Part
		public static boolean uploadVideoPart(String video_id, String upload_token, int part_number, int content_length, String file_path) throws UnirestException, IOException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.put(String.format("https://uploads.twitch.tv/upload/%s?part=%d&upload_token=%s", video_id, part_number, upload_token)).header("Content-Length", Integer.toString(content_length)).body(Files.readAllBytes(Paths.get(file_path))).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("uploadVideoPart", response))
				return false;
			return true;
		}
		
		//Complete Video Upload
		public static boolean uploadVideoComplete(String video_id, String upload_token, int content_length, String file_path) throws UnirestException, IOException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = Unirest.post(String.format("https://uploads.twitch.tv/upload/%s/complete?upload_token=%s", video_id, upload_token)).asJsonAsync();
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("uploadVideoPart", response))
				return false;
			return true;
		}
		
		//Update Video #requires auth
		//Multiple Methods for optional parameters
		public static models.api.videos.Video updateVideo(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPut(String.format("https://api.twitch.tv/kraken/videos/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateVideo", response)) 
				return null;
			return new models.api.videos.Video(response.getBody().getObject());
		}
		
		public static models.api.videos.Video updateVideo(String id, Map<String, Object> parameters) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = authPut(String.format("https://api.twitch.tv/kraken/videos/%s", id), parameters);
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("updateVideo", response)) 
				return null;
			return new models.api.videos.Video(response.getBody().getObject());
		}
		
		//Delete Video #requires auth
		public static boolean deleteVideo(String id) throws UnirestException, InterruptedException, ExecutionException {
			Future<HttpResponse<JsonNode>> future = delete(String.format("https://api.twitch.tv/kraken/videos/%s", id));
			
			HttpResponse<JsonNode> response = future.get();
			if (errorCheck("deleteVideo", response))
				return false;
			return true;
		}
	}
}
